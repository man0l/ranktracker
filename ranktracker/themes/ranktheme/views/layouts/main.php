<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
        <!-- Hammer reload -->
          <script>
          /*
            setInterval(function(){
              try {
                if(typeof ws != 'undefined' && ws.readyState == 1){return true;}
                ws = new WebSocket('ws://'+(location.host || 'localhost').split(':')[0]+':35353')
                ws.onopen = function(){ws.onclose = function(){document.location.reload()}}
                ws.onmessage = function(){
                  var links = document.getElementsByTagName('link'); 
                    for (var i = 0; i < links.length;i++) { 
                    var link = links[i]; 
                    if (link.rel === 'stylesheet' && !link.href.match(/typekit/)) { 
                      href = link.href.replace(/((&|\?)hammer=)[^&]+/,''); 
                      link.href = href + (href.indexOf('?')>=0?'&':'?') + 'hammer='+(new Date().valueOf());
                    }
                  }
                }
              }catch(e){}
            }, 1000)
            */
          </script>
        <!-- /Hammer reload -->
      

  <link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/fullcalendar.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/datatables/bootstrap.datatables.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/chosen.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/jquery.timepicker.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/colpick.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/dropzone.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/jquery.handsontable.full.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/jscrollpane.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/jquery.pnotify.default.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/plugins/jquery.pnotify.default.icons.css">
<link rel="stylesheet" href="<?php echo $baseUrl?>/css/app.css">

<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,700|Roboto+Condensed:300,400,700' rel='stylesheet' type='text/css'>

  <link href="<?php echo $baseUrl?>/assets/favicon.ico" rel="shortcut icon">
  <link href="<?php echo $baseUrl?>/assets/apple-touch-icon.png" rel="apple-touch-icon">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    @javascript html5shiv respond.min
  <![endif]-->

  <title><?php echo CHtml::encode($this->pageTitle); ?></title>

<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style>

</head>


<body class="glossed" style="">
<div class="all-wrapper fixed-header left-menu">
  <div class="page-header">
  <div class="header-links hidden-xs">
    <div class="top-search-w pull-right">
      <input type="text" class="top-search" placeholder="Search">
    </div>

    <div class="dropdown">
      <a href="#" class="header-link clearfix" data-toggle="dropdown">
                <div class="user-name-w">
          Mijn account <i class="fa fa-caret-down"></i>
        </div>
      </a>
      <ul class="dropdown-menu dropdown-inbar">
            
        <li><a href="#"><i class="fa fa-cog"></i> Mijn instellingen</a></li>
        <li><a href="<?php echo Yii::app()->createUrl('/site/logout')?>"><i class="fa fa-power-off"></i> Uitloggen</a></li>
      </ul>
    </div>
  </div>
  <a class="current logo hidden-xs" href="<?php echo Yii::app()->createUrl('/site/index')?>"><img src="<?php echo $baseUrl?>/assets/images/logo-icoon.png" width="60%" heigth="60%" alt=""></a>
  <a class="menu-toggler" href="javacript:;"><i class="fa fa-bars"></i></a>
  <h1>Dashboard</h1>
</div>

  <!--  start vertical nav -->
  <div class="side">
  <div class="sidebar-wrapper">
  <ul>
    <li <?php if($this->uniqueid == 'site') :?>class="current"<?php endif?>>
      <a <?php if($this->uniqueid == 'site') :?>class="current"<?php endif?> class="current" href="<?php echo Yii::app()->createUrl('/site/index') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
        <i class="fa fa-tachometer"></i>
      </a>
    </li>
    <li <?php if($this->uniqueid == 'domain') :?>class="current"<?php endif?>>
      <a <?php if($this->uniqueid == 'domain') :?>class="current"<?php endif?> href="<?php echo Yii::app()->createUrl('/domain/list') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Domeinen">
        <i class="fa fa-bar-chart-o"></i>  
      </a>
    </li>
    
  </ul>
</div> 
<!-- /vertical nav -->
<!--  submenu -->
  <div class="sub-sidebar-wrapper">
  <?php $this->widget('zii.widgets.CMenu', array(
  	'items'	=> $this->menu
	));
  
  ?>
   
</div>
<!-- /submenu -->
  </div>
  

  
  <!-- start main content -->
  <div class="main-content">
  		<?php echo $content?> 		
  </div>
    <!-- end main content -->
<div class="row">



  
  <div class="page-footer">
  © 2014 - Simpel SEO - Onderdeel van Simpel Ltd.
</div>
</div>



<script src="<?php echo $baseUrl?>/js/jquery.min.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery-ui.min.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.pnotify.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.sparkline.min.js"></script>
<script src="<?php echo $baseUrl?>/js/mwheelIntent.js"></script>
<script src="<?php echo $baseUrl?>/js/mousewheel.js"></script>
<script src="<?php echo $baseUrl?>/js/tab.js"></script>
<script src="<?php echo $baseUrl?>/js/dropdown.js"></script>
<script src="<?php echo $baseUrl?>/js/tooltip.js"></script>
<script src="<?php echo $baseUrl?>/js/collapse.js"></script>
<script src="<?php echo $baseUrl?>/js/scrollspy.js"></script>
<script src="<?php echo $baseUrl?>/js/bootstrap-datepicker.js"></script>
<script src="<?php echo $baseUrl?>/js/transition.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.knob.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.flot.min.js"></script>
<script src="<?php echo $baseUrl?>/js/fullcalendar.js"></script>
<script src="<?php echo $baseUrl?>/js/datatables.min.js"></script>
<script src="<?php echo $baseUrl?>/js/chosen.jquery.min.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.timepicker.min.js"></script>
<script src="<?php echo $baseUrl?>/js/daterangepicker.js"></script>
<script src="<?php echo $baseUrl?>/js/colpick.js"></script>
<script src="<?php echo $baseUrl?>/js/moment.min.js"></script>
<script src="<?php echo $baseUrl?>/js/bootstrap.datatables.js"></script>
<script src="<?php echo $baseUrl?>/js/modal.js"></script>
<script src="<?php echo $baseUrl?>/js/raphael-min.js"></script>
<script src="<?php echo $baseUrl?>/js/morris-0.4.3.min.js"></script>
<script src="<?php echo $baseUrl?>/js/justgage.1.0.1.min.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.maskmoney.js"></script>
<script src="<?php echo $baseUrl?>/js/summernote.js"></script>
<script src="<?php echo $baseUrl?>/js/dropzone-amd-module.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.validate.min.js"></script>
<script src="<?php echo $baseUrl?>/js/jquery.bootstrap.wizard.min.js"></script>
<script src="<?php echo $baseUrl?>/js/jscrollpane.min.js"></script>
<script src="<?php echo $baseUrl?>/js/application.js"></script>
<script src="<?php echo $baseUrl?>/js/clientside_validation.js"></script>

<!--  <script src="<?php echo $baseUrl?>/js/dashboard.js"></script> -->
<script type="text/javascript">
(function() {
	  $(function() {});

	}).call(this);

$(function () {
    $("[rel='tooltip']").tooltip();
});

</script>
	
</script>
<!-- @include _footer-->
</body></html>