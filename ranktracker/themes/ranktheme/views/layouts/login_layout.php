<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php $baseUrl = Yii::app()->theme->baseUrl; ?>
  
        <!-- Hammer reload -->
          <script>
            setInterval(function(){
              try {
                if(typeof ws != 'undefined' && ws.readyState == 1){return true;}
                ws = new WebSocket('ws://'+(location.host || 'localhost').split(':')[0]+':35353')
                ws.onopen = function(){ws.onclose = function(){document.location.reload()}}
                ws.onmessage = function(){
                  var links = document.getElementsByTagName('link'); 
                    for (var i = 0; i < links.length;i++) { 
                    var link = links[i]; 
                    if (link.rel === 'stylesheet' && !link.href.match(/typekit/)) { 
                      href = link.href.replace(/((&|\?)hammer=)[^&]+/,''); 
                      link.href = href + (href.indexOf('?')>=0?'&':'?') + 'hammer='+(new Date().valueOf());
                    }
                  }
                }
              }catch(e){}
            }, 1000)
          </script>
        <!-- /Hammer reload -->
      

  <link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/fullcalendar.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/datatables/datatables.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/datatables/bootstrap.datatables.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/chosen.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/jquery.timepicker.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/daterangepicker-bs3.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/colpick.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/dropzone.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/jquery.handsontable.full.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/jscrollpane.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/jquery.pnotify.default.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/plugins/jquery.pnotify.default.icons.css">
<link rel="stylesheet" href="http://simpelseo.com/client/assets/css/app.css">
 
  <link href="http://simpelseo.com/client/assets/favicon.ico" rel="shortcut icon">
  <link href="http://simpelseo.com/client/assets/apple-touch-icon.png" rel="apple-touch-icon">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    @javascript html5shiv respond.min
  <![endif]-->

  <title>Responsive Admin template based on Bootstrap 3</title>

<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>

<body class="glossed" style="">
<div class="all-wrapper no-menu-wrapper light-bg">
  <div class="login-logo-w">
    <img src="<?php echo $baseUrl?>/assets/images/logo-simpelseo.png" width="20%" heigth="20%">
  </div>
  <div class="row">
    <div class="col-md-4 col-md-offset-4">

      <div class="widget widget-blue">
        <div class="widget-title">
          <h3 class="text-center"><i class="fa fa-lock"></i> Welkom terug! U kunt hier inloggen.</h3>
        </div>
        <div class="widget-content clearfix">
          <?php echo $content;?>
        </div>
        <br style="clear:both;">
      </div>
    </div>
  </div>
</div>
</body></html>