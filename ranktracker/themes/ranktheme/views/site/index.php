<?php 
/* @var $this SiteController */
$baseUrl = Yii::app()->theme->baseUrl; 
?>

<ol class="breadcrumb" id="anchor_inleiding">
  <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Home</a></li>
  <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Dashboard</a></li>
  <li class="active">Inleiding</li>
</ol>

<div class="alert alert-info alert-dismissable">
              <i class="fa fa-lightbulb"></i> <strong>Welkom!</strong> <br><br>
              Dit is het dashboard van Simpel SEO V1.0. Op deze pagina krijgt u een snel overzicht van uw zoekwoorden en domeinen. Op deze manier heeft u meteen een idee welke zoekwoorden het meeste gedaald zijn of net het hardst gestegen...Wij staan altijd open voor suggesties!<br><br>Veel succes!<br>Simpel SEO Team
              
            </div>

<div class="widget widget-blue">
      <span class="offset_anchor" id="anchor_algemeen"></span>
      <div class="widget-title">
        <div class="widget-controls">
       </div>
        <h3><i class="fa fa-bar-chart-o"></i> ALGEMENE STATISTIEKEN</h3>
      </div>
      <div class="widget-content">
        <div class="row">
          <div class="col-lg-3 col-md-4 col-sm-6 text-center">
            <div class="widget-content-blue-wrapper changed-up">
              <div class="widget-content-blue-inner padded">
                <div class="pre-value-block"><i class="fa fa-globe"></i> Aantal Domeinen</div>
                <div class="value-block">
                  <div class="value-self"><?php echo $domainsCount?> / ∞</div>
                </div>
                </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 text-center">
            <div class="widget-content-blue-wrapper changed-up">
              <div class="widget-content-blue-inner padded">
                <div class="pre-value-block"><i class="fa fa-search"></i> Aantal Zoekwoorden</div>
                <div class="value-block">
                  <div class="value-self"><?php echo $keywordsCount?> / 50</div>
                  <div class="value-sub"><br>Meer zoekwoorden?<br><a href="#">Upgrade naar Pro!</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 text-center hidden-md">
            <div class="widget-content-blue-wrapper changed-up">
              <div class="widget-content-blue-inner padded">
                <div class="pre-value-block"><i class="fa fa-arrow-up"></i> Zoekwoorden gestegen</div>
                <div class="value-block">
                  <div class="value-self"><font color="green"><?php echo $keywordsUpCount?></font></div>
                  <div class="value-sub"><br>Sinds laatste update</div>
                  <div class="value-sub"><?php echo Yii::app()->dateFormatter->formatDateTime($lastUpdate);?></div>
                </div>
              
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-6 text-center">
            <div class="widget-content-blue-wrapper changed-up">
              <div class="widget-content-blue-inner padded">
                <div class="pre-value-block"><i class="fa fa-arrow-down"></i> Zoekwoorden gedaald</div>
                <div class="value-block">
                  <div class="value-self"><font color="red"><?php echo $keywordsDownCount?></font></div>
                  <div class="value-sub"><br>Sinds laatste update</div>
                  <div class="value-sub"><?php echo Yii::app()->dateFormatter->formatDateTime($lastUpdate);?></div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="row">
  <div class="col-md-6">
    <div class="widget widget-green">
      <span class="offset_anchor" id="anchor_gestegen_zoekwoorden"></span>
      <div class="widget-title">
        <div class="widget-controls">
		</div>
        <h3><i class="fa fa-arrow-up"></i> Gestegen Zoekwoorden</h3>
      </div>
      <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Domeinnaam</th>
                  <th>Zoekwoord</th>
                  <th><center>Huidig</center></th>
                  <th><center>Vorige</center></th>
                </tr>
              </thead>
              <tbody>
                      
              <?php foreach ($keywordsUp as $position) :?>
                <tr>
                  <td><?php echo $position->domain->name?></td>
                  <td><?php echo $position->keywords->keyword?></td>
                  <td><center><font color="green"><?php echo $position->position?></font></center></td>
                  <td><center><?php echo $position->position_week?></center></td>
                </tr>
               <?php endforeach;?>         
              </tbody>
            </table>
            </div>
            <div class="row">
              

            </div>
          
        
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="widget widget-red">
      <span class="offset_anchor" id="anchor_gedaalde_zoekwoorden"></span>
      <div class="widget-title">
        <div class="widget-controls">
  
</div>
        <h3><i class="fa fa-arrow-down"></i> Zoekwoorden Gedaald</h3>
      </div>
      <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Domeinnaam</th>
                  <th>Zoekwoord</th>
                  <th><center>Huidig</center></th>
                  <th><center>Vorige</center></th>
                </tr>
              </thead>
              <tbody>
               <?php foreach ($keywordsDown as $position) :?>
                <tr>
                  <td><?php echo $position->domain->name?></td>
                  <td><?php echo $position->keywords->keyword?></td>
                  <td><center><font color="red"><?php echo $position->position?></font></center></td>
                  <td><center><?php echo $position->position_week?></center></td>
                </tr>
               <?php endforeach;?>
                 
              </tbody>
            </table>
            </div>
        
      </div>
    </div>
  </div>
</div>

<div class="widget widget-blue">
      <span class="offset_anchor" id="anchor_pakketten"></span>
      <div class="widget-title">
        <div class="widget-controls">
       </div>
        <h3><i class="fa fa-dropbox"></i> PAKKETTEN</h3>
      </div>
      <div class="widget-content">

        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Account Type</th>
              <th>Domeinen</th>
              <th>Zoekwoorden</th>
              <th><center>Updates/dag</center></th>
              <th><center>Handmatige updates</center></th>
              <th>Statistieken</th>
              <th>Rapportage</th>         
              <th>Prijs</th>
              <th>Actie</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><b>Gratis</b> (Huidig)</td>
              <td>Ongelimiteerd</td>
              <td>50</td>
              <td><center>1</center></td>
              <td><center>0</center></td>
              <td>Basis</td>
              <td>Dagelijks</td>
              <td>€ 0,00</td>
              <td><b>Upgraden!</b></td>
            </tr>
            <tr>
              <td>Pro</td>
              <td>Ongelimiteerd</td>
              <td>300</td>
              <td><center>2</center></td>
              <td><center>1</center></td>
              <td>Uitgebreid</td>
              <td>Dagelijks</td>
              <td>€ 7,50</td>
              <td>Upgraden!</td>
            </tr>
            <tr>
              <td>All-in</td>
              <td>Ongelimiteerd</td>
              <td>Ongelimiteerd</td>
              <td><center>2</center></td>
              <td><center>3</center></td>
              <td>Uitgebreid</td>
              <td>Dagelijks</td>
              <td>€ 25,00</td>
              <td>Upgraden!</td>
            </tr>
          </tbody>
        </table>
      </div>
          </div>
        </div>