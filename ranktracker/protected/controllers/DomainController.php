<?php

class DomainController extends Controller
{
	public function actionList()
	{
		
		$this->getMenu();
		//$positions = Positions::model()->with('domain','keywords')->findAll(array('condition' => 't.user_id='.Yii::app()->user->id));
		$check = Positions::model()->find(array(
				'condition' => "user_id = :user_id",
				'params' => array(":user_id" => Yii::app()->user->id),
				'order' => 'date_at DESC',
					
		));
		
		$date = new DateTime($check->date_at);
		
		$dateAt = $date->format("Y-m-d"); 
		
		$keywords = Keyword::model()->with('domain', 'results')->findAll(array(
				'condition' =>'results.user_id = :user_id AND results.date_at >=  :date_at ',
				'params'    => array('user_id' => Yii::app()->user->id, 'date_at' => $dateAt)
				
		));
		 
		$allSearchEngines = SearchEngines::model()->findAll();		
	 	
		$this->render('list', array('positions' => $pos, 'keywords' => $keywords, 'allSearchEngines' => $allSearchEngines));
	}
	
	public function actionAdd() 
	{
		$this->getMenu();
		
		$model = new AddProjectForm();
		$errorFlag = 0; 
		if($_POST) {
			
		 
			$model->attributes = $_POST['AddProjectForm'];
			
			$state = (int) $this->check200State($_POST['AddProjectForm']['domain']);
			
						 
				if($model->validate()) {
					
					
					if($state !== 200)
					{
						$errorFlag = 1;
						$errors = array(array('This domain does not exists.'));
					} else 
					{
						// process the data
						$this->processForm();
						
						Yii::app()->user->setFlash('success', 'The keywords were added successfully.');
						$this->redirect(array('domain/list'));
					}
					
				} else {
					$errors = $model->getErrors();
					$errorFlag = 1;
				}
			
			
		}	
		
		$searchEngines = SearchEngines::model()->findAll();
			
		$this->render('add', array('searchEngines' => $searchEngines, 'model' => $model, 'errorFlag' => $errorFlag, 'errors' => $errors ));
	}
	
	function check200State($domain) 
	{
		$ch = curl_init($domain);
		
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($ch);
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return  $http_status;
		
	}	

	
	function processForm() 
	{
		
				
		$domainCheck = Domain::model()->findByAttributes(array(
			'name'	=> $_POST['AddProjectForm']['domain']				
		));
		
		if(!$domainCheck) {
			$domain = new Domain;
			$domain->user_id = Yii::app()->user->id;
			$domain->name = $_POST['AddProjectForm']['domain'];
			$domain->created_at = date("Y-m-d H:i:s");
			$domain->colorPick = $_POST['AddProjectForm']['colorPick'];
			$domain->save();	
		} else $domain = $domainCheck;		
			
		$keywords = preg_split('/\n|\r\n?/', $_POST['AddProjectForm']['keywords']);
			
		foreach($_POST['AddProjectForm']['searchEngine'] as $se) {
			$searchEngine = new SearchEnginesDomain;
			$searchEngine->search_engines_id = $se;
			$searchEngine->domain_id = $domain->id;
			$searchEngine->save();
				
		}
			
		foreach($keywords as $kwd) {
			if(trim($kwd)) {
				
				$kwdCheck = Keyword::model()->findByAttributes(array(
					'domain_id' => $domain->id,
					'keyword'	=> $kwd
				));
				
				if(!$kwdCheck) {
					$keyword = new Keyword();
					$keyword->domain_id =  $domain->id;
					$keyword->keyword = $kwd;
					$keyword->save();
				} else $keyword = $kwdCheck;
					
				$allKwd[] = $keyword->id;
		
			}
		}
		
		$check = Positions::model()->find(array(
				'condition' => "user_id = :user_id",
				'params' => array(":user_id" => Yii::app()->user->id),
				'order' => 'date_at DESC',
					
		));
		
		 
		if($check)
		{
			$dateAt = $check->date_at;
		} else {
			$dateAt = date("Y-m-d H:i:s");
		} 
		
		
			
		foreach($_POST['AddProjectForm']['searchEngine'] as $sengine) {
			foreach($allKwd as $secondaryKeyword) {
				
				$resCheck = Positions::model()->findByAttributes(array(
					'keywords_id' => $secondaryKeyword,
					'search_engine_id' => $sengine,
					'domain_id' => $domain->id	
				));
				
				if(!$resCheck) {					
				
					$result = new Positions();
					$result->keywords_id = $secondaryKeyword;
					$result->search_engine_id = $sengine;
					$result->domain_id = $domain->id;
					$result->user_id = Yii::app()->user->id;
					$result->position = 0;
					$result->date_at = $dateAt;
					$result->is_exported = 0;
					$result->position_month = 0;
					$result->position_week = 0;
					$result->save();
				}
				
			  
			}			
		}
		
	 
		
	}
	
	public function actionDetails()
	{
		
		$this->getMenu();
		$keywordId = $_GET['id'];
		$allSearchEngines = SearchEngines::model()->findAll();
		
		$connection=Yii::app()->db;
		// get the search engines for the keyword
		$sql = "SELECT DISTINCT search_engine_id FROM results WHERE keywords_id = :keywords_id";
		$cmd = $connection->createCommand($sql);
		$cmd->bindParam(":keywords_id", $keywordId, PDO::PARAM_STR);
		$rows = $cmd->queryAll();
		
		$countRows = count($rows);
		
		if($rows) 
		{
			foreach($rows as $row)
			{
				
				$sql = "SELECT DISTINCT DATE_FORMAT(date_at, '%Y-%m-%d') date FROM results WHERE keywords_id = :keywords_id AND search_engine_id = :search_engine_id ORDER BY date_at DESC LIMIT 7";
				$cmd = $connection->createCommand($sql);
				$cmd->bindParam(":keywords_id", $keywordId, PDO::PARAM_STR);
				$cmd->bindParam(":search_engine_id", $row['search_engine_id'], PDO::PARAM_STR);
				
				$dateRows = $cmd->queryAll();
				foreach($dateRows as $dateRow) 
				{
					 
					$results[$row['search_engine_id']][$dateRow['date']]

					 = Positions::model()->findAll(array(
							"condition" => "keywords_id = :keywords_id AND search_engine_id = :search_engine_id AND DATE_FORMAT(date_at, '%Y-%m-%d') = :date_at ",
					 		"params" => array(
					 			':keywords_id' => $keywordId,
					 			':search_engine_id' => $row['search_engine_id'],
					 			':date_at'	=> $dateRow['date'] 
					 		)
					 		
					));
				}
				
			}
		}
		
		$keyword = Keyword::model()->findByAttributes(array('id' => $keywordId));
		
		$check = Positions::model()->find(array(
				'condition' => "user_id = :user_id",
				'params' => array(":user_id" => Yii::app()->user->id),
				'order' => 'date_at DESC',
					
		));
		
		$date = new DateTime($check->date_at);
		
		$dateAt = $date->format("Y-m-d");
		
		$keywords[] = Keyword::model()->with('results')->find(array(
				'condition' => "t.id = :keyword_id AND results.date_at >= :date_at",
				'params' => array(":keyword_id" => $keywordId, 'date_at' => $dateAt." 00:00:00"),
		));
		
	 
		 
		
		$this->render("keywordDetails", array(
			'results' 	=> $results,
			'countRows' => $countRows,
			'keyword'	=> $keyword,
			'keywords'	=> $keywords,
			'allSearchEngines'	=> $allSearchEngines
		));
		
	}
	
	public function actionDetails1() 
	{
		$keywordId = $_GET['id'];
		$allSearchEngines = SearchEngines::model()->findAll();
		
		$connection=Yii::app()->db;
		$sql = "SELECT DISTINCT DATE_FORMAT(date_at, '%Y-%m-%d') date FROM results WHERE keywords_id = :keywords_id ";
		
		
		if($_POST)
		{
			if($_POST['fromDate'] && !$_POST['toDate'])
			{
				$sql .= " AND date_at > :fromDate ORDER BY date_at DESC";
			
			} else if($_POST['toDate'] && !$_POST['fromDate'])
			{
				$sql .= " AND ( date_at < :dateTo  ) ORDER BY date_at DESC";
				
		
			}  else if($_POST['toDate'] && $_POST['fromDate'])
			{
				$sql .= " AND (date_at > :dateFrom AND date_at < :dateTo  ) ORDER BY date_at DESC";
				
			}
		} else {
			$sql .= " ORDER BY date_at DESC LIMIT 7";
		}
			
		$cmd = $connection->createCommand($sql);
		$cmd->bindParam(":keywords_id", $keywordId, PDO::PARAM_STR);
		
		if($_POST)
		{
			if($_POST['fromDate']) {
				list($m, $d, $y) = explode("/", $_POST['fromDate']);				
				$_POST['fromDate'] = sprintf("%s-%s-%s", $y, $m, $d);
			}
			
			if($_POST['toDate']) {
				list($m, $d, $y) = explode("/", $_POST['toDate']);				
				$_POST['toDate'] = sprintf("%s-%s-%s", $y, $m, $d);
			}
			
			if($_POST['fromDate'] && !$_POST['toDate'])
			{
				
				$cmd->bindParam(":dateFrom", $_POST['fromDate']);
				 
			} else if($_POST['toDate'] && !$_POST['fromDate'])
			{
				$cmd->bindParam(":dateTo", $_POST['toDate']);
				 
		
			}  else if($_POST['toDate'] && $_POST['fromDate'])
			{
				$cmd->bindParam(":dateTo", $_POST['toDate']);
				$cmd->bindParam(":dateFrom", $_POST['fromDate']);
				
			}
		}
		 
		
		$rows = $cmd->queryAll();
			
		$dates = array();
		/*
		foreach($rows as $row)
		{
			$date = new DateTime($row['date']." 00:00:00");
				
			$dateAt = $date->format("Y-m-d");
			
			// search engines cycle
			
			$sql = "SELECT DISTINCT search_engine_id FROM results WHERE keywords_id = :keywords_id";
			$cmd = $connection->createCommand($sql);
			$cmd->bindParam(":keywords_id", $keywordId, PDO::PARAM_STR);
			
			$searchEngineRows = $cmd->queryAll();
			
		 
			
			foreach($searchEngineRows as $seRow)
			{
				 
				
				$results = Positions::model()->with('search_engine')->findAll(array(
						'condition'	=> "search_engine_id = :search_engine_id AND DATE_FORMAT(date_at, '%Y-%m-%d') = :date_at",
						"params"	=> array(":search_engine_id" => $seRow['search_engine_id'], ":date_at" => $dateAt)
				));
				
				foreach($results as $result)
				{
					$dates[$row['date']]['search_engine_'.$result->search_engine_id] = $result->position;
					$labels[$row['date']]['search_engine_'.$result->search_engine_id] = $result->search_engine->name;
				}
				
			}
			
			 $keywordsCount = Keyword::model()->with('results')->count(array(
					'condition' =>'results.user_id = :user_id AND results.date_at >=  :date_at AND results.keywords_id = :keywords_id',
					'params'    => array('user_id' => Yii::app()->user->id, 'date_at' => $dateAt, 'keywords_id' => $keywordId)
			
			));		 
			 
			
		}
		*/
		
		foreach($rows as $row)
		{
			$date = new DateTime($row['date']." 00:00:00");
				
			$dateAt = $date->format("Y-m-d");
			
		
			$sql = "SELECT DISTINCT search_engine_id FROM results WHERE keywords_id = :keywords_id";
			$cmd = $connection->createCommand($sql);
			$cmd->bindParam(":keywords_id", $keywordId, PDO::PARAM_STR);
				
			$searchEngineRows = $cmd->queryAll();
				
			foreach($searchEngineRows as $seRow)
			{
				$results = Positions::model()->with('search_engine')->findAll(array(
						'condition'	=> "search_engine_id = :search_engine_id AND DATE_FORMAT(date_at, '%Y-%m-%d') = :date_at AND keywords_id = :keywords_id",
						"params"	=> array(":search_engine_id" => $seRow['search_engine_id'], ":date_at" => $dateAt, ":keywords_id" => $keywordId)
				));
				
			 	print_r($results);
				
				$i = 0;
				foreach($results as $result)
				{
					$dates[$row['date']]['search_engine_'.$result->search_engine_id] = $result->position;
					$labels[$row['date']]['search_engine_'.$result->search_engine_id] = $result->search_engine->name;
						
					$i++;
				}
				
				$datesAll[] = $dates;
				$labelsAll[] = $labels;
			}
			
		}
		 print_r($datesAll);exit;
		
		$keywordsCount = Keyword::model()->with('results')->count(array(
				'condition' =>'results.user_id = :user_id AND results.date_at >=  :date_at AND results.keywords_id = :keywords_id',
				'params'    => array('user_id' => Yii::app()->user->id, 'date_at' => $dateAt, 'keywords_id' => $keywordId)
					
		));
		
		$allSearchEngines = SearchEngines::model()->findAll();
		
		$keyword = Keyword::model()->with('results')->find(array(
				'condition' => "t.id = :keyword_id AND DATE_FORMAT(results.date_at, '%Y-%m-%d') = :date_at",
				'params' => array(":keyword_id" => $keywordId, ':date_at' => $dateAt),
		));
		
		$keywords[] = $keyword;		
		$this->getMenu();
		
		
		$this->render("details", array(
				'keyword' => $keyword,				 
				'keywords'	=> $keywords,
				'allSearchEngines'	=> $allSearchEngines,
				/* 'count' => $count,
				'countTop10' => $countTop10,
				'countTop5'  => $countTop5,
				'keywords'	=> $keywords,
				'allSearchEngines'	=> $allSearchEngines, */
				'dates'	=> $dates,
				'labels' => $labels,
				'keywordsCount' => $keywordsCount,
				'labelsAll' => $labelsAll,
				'datesAll' => $datesAll
		));		
		
	}
	
	public function actionkeywordDetails()
	{
		
		$this->getMenu();
		
		$id = $_GET['id'];		
		
		
		$keyword = Keyword::model()->find(array(
				'condition' => "domain_id = :domain_id",
				'params' => array(":domain_id" => $id),
		));	
	 
		
		$params = array(
				'condition' => "domain_id = :domain_id",
				'params' => array(":domain_id" => $id),
				'order' => 'date_at ASC',
				//'limit' => 30,
				'group' => 'date_at'
			
		);
		
		// date filter
		if($_POST) 
		{
			if($_POST['fromDate'] && !$_POST['toDate']) 
			{
				$params['condition'] = "domain_id = :domain_id AND date_at > :fromDate";
				$params['params']	 = array(':domain_id' => $id,'fromDate' => $_POST['fromDate']);
			} else if($_POST['toDate'] && !$_POST['fromDate'])
			{
				$params['condition'] = "domain_id = :domain_id AND ( date_at < :dateTo  )";
				$params['params']	 = array(':domain_id' => $id, ':dateTo' => $_POST['toDate']);
				
			}  else if($_POST['toDate'] && $_POST['fromDate'])
			{
				$params['condition'] = "domain_id = :domain_id AND (date_at > :dateFrom AND date_at < :dateTo  )";
				$params['params']	 = array(':domain_id' => $id, ':dateFrom' => $_POST['fromDate'], ':dateTo' => $_POST['toDate']);
			}
		}
		
		$results = Positions::model()->findAll($params);
		
		
		
		if(!$results)
		{
			// get the default data
			$results = Positions::model()->findAll(array(
					'condition' => "domain_id = :domain_id",
					'params' => array(":domain_id" => $id),
					'order' => 'date_at ASC',
					//'limit' => 30,
					'group' => 'date_at'					
			));
		}
		
		$check = Positions::model()->find(array(
				'condition' => "user_id = :user_id",
				'params' => array(":user_id" => Yii::app()->user->id),
				'order' => 'date_at DESC',
					
		));
		
		$date = new DateTime($check->date_at);
		
		$dateAt = $date->format("Y-m-d");
		
		$count = Positions::model()->count(array(
			'condition' => "user_id = :user_id AND date_at >= :date_at",
			'params' => array(":user_id" => Yii::app()->user->id, 'date_at' => $dateAt),	
		
		));
		
		$countTop10 = Positions::model()->count(array(
				'condition' => "user_id = :user_id AND date_at >= :date_at AND position <= 10",
				'params' => array(":user_id" => Yii::app()->user->id, 'date_at' => $dateAt),
		
		));
		
		$countTop5 = Positions::model()->count(array(
				'condition' => "user_id = :user_id AND date_at >= :date_at AND position <= 5",
				'params' => array(":user_id" => Yii::app()->user->id, 'date_at' => $dateAt),
		
		));		
		
		
		
		$keywords = Keyword::model()->with('domain', 'results')->findAll(array(
				'condition' =>'results.user_id = :user_id AND results.date_at >=  :date_at AND results.domain_id = :domain_id',
				'params'    => array('user_id' => Yii::app()->user->id, 'date_at' => $dateAt, 'domain_id' => $id)
		
		));
		
			
			
		$allSearchEngines = SearchEngines::model()->findAll();
		
		$connection=Yii::app()->db; 
		$sql = "SELECT DISTINCT DATE_FORMAT(date_at, '%Y-%m-%d') date FROM results WHERE domain_id = :domain_id ORDER BY date_at DESC LIMIT 7";
		$cmd = $connection->createCommand($sql);
		$cmd->bindParam(":domain_id", $id, PDO::PARAM_STR);
		
		$rows = $cmd->queryAll();
		 
		$dates = array();
		foreach($rows as $row)
		{
			$date = new DateTime($row['date']."00:00:00");
			
			$dateAt = $date->format("Y-m-d");
			
			 $results = Positions::model()->with('keywords')->findAll(array(
				'condition' => "user_id = :user_id AND DATE_FORMAT(date_at, '%Y-%m-%d') = :date_at",
				'params' => array(":user_id" => Yii::app()->user->id, 'date_at' => $dateAt),
				'group'	=> 'keywords_id'
			));

			 foreach($results as $result)
			 {
			 	$dates[$row['date']]['kwd'.$result->keywords_id] = $result->position;
			 	$labels[$row['date']][] = $result->keywords->keyword;
			 }

			 
		}
		
	 
		
		$this->render("keywordDetails", array(
							'keyword' => $keyword, 
							'results' => $results, 
							'count' => $count,
							'countTop10' => $countTop10,
							'countTop5'  => $countTop5,
							'keywords'	=> $keywords,
							'allSearchEngines'	=> $allSearchEngines,
							'dates'	=> $dates,
							'labels' => $labels		
					));
		
	}
	
	function getMenu()
	{
		return $this->menu = $menu = array(
				array('label' => 'OVERZICHT', 				'url' => 	array('/domain/list','#'=>'anchor_domein_overzicht')),
				array('label' => 'NIEUW DOMEIN TOEVOEGEN',	'url' => 	array('/domain/add','#' => 'anchor_algemeen')),
			
		);
	}
	
	public function filters()
	{
		return array('accessControl');
	}
	
	public function accessRules()
	{
		return array(
            array('allow', // allow authenticated users to access all actions
                'users'=>array('@'),
            ),
            array('deny'),
        );
		
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}