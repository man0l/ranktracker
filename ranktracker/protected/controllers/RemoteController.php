<?php

class RemoteController extends Controller
{
	/**
	 * run this method to update the positions from VPS2 to VPS1
	 */
	public function actionImport()
	{
		$results = Positions::model()->findAllByAttributes(array('is_exported' => false, 'is_parsed' => 1));
		$exportJson = array();
			
		if($results)
		{
			foreach($results as $result)
			{
				$exportJson[] = array(
						'result_id' => $result->id,
						'keyword' => $result->keywords->keyword,
						'domain'  => $result->domain->name,
						'search_engine' => $result->search_engine->id,
						'user_id'	=> $result->user_id,	
						'position'  => $result->position,
						'is_parsed' => $result->is_parsed
							
				);
				
				$result->is_exported = 1;
				$result->save();
				
				
			}
		}
		
		$json = json_encode($exportJson);
		
		header('Content-type: application/json');
		
		echo $json;
		
		Yii::app()->end();
		
	}
	
	/**
	 * Run this method in the destination server ( where the app is hosted, VPS 1 )
	 */
	public function actionExport() 
	{
		$results = Positions::model()->findAllByAttributes(array('is_exported' => false));
		$exportJson = array();
			
		if($results)
		{
			foreach($results as $result)
			{
				$exportJson[] = array(
						'result_id' => $result->id,
						'keyword' => $result->keywords->keyword,
						'domain'  => $result->domain->name,
						'search_engine' => $result->search_engine->id,
						'user_id'	=> $result->user_id,	
							
				);
				
				$result->is_exported = 1;
				$result->save();
			}
		}
		
		$json = json_encode($exportJson);
		
		header('Content-type: application/json');
		
		echo $json;
		
		Yii::app()->end();
	}
	

	
}