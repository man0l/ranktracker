<?php 
	$baseUrl = Yii::app()->theme->baseUrl;
?>		
		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-singUp-form',
	'enableAjaxValidation'=>false,
)); ?>	
		
		
	<?php 
    // if site is not invitation based, then do not display invitation code errors... :)
    if(Yii::app()->getModule('bum')->invitationBasedSignUp):
        echo $form->errorSummary(array($model, $model->invitations)); 
    else:
        echo $form->errorSummary(array($model)); 
    endif;
    
    ?>

            <?php $this->widget('facebook_app', array(
                'appId'=>Yii::app()->getModule('bum')->fb_appId,
                'secret'=>Yii::app()->getModule('bum')->fb_secret,
                'text'=>'Sign in with <b>Facebook</b>',
                'target'=>'_self',
            )); ?>
            
            
            <div class="lined-separator">Of maak een account aan</div>
            <div class="form-group relative-w">               
              <?php echo $form->textField($model,'name', array('class' => 'form-control', 'placeholder'=> 'Bedrijfsnaam / Naam', 'type' => 'text')); ?>
              <?php echo $form->error($model,'name'); ?>           
              <i class="fa fa-user input-abs-icon"></i>              
            </div>
            
            <div class="form-group relative-w">               
              <?php echo $form->textField($model,'user_name', array('class' => 'form-control', 'placeholder'=> 'Gebruiker', 'type' => 'text')); ?>
              <?php echo $form->error($model,'user_name'); ?>           
              <i class="fa fa-user input-abs-icon"></i>               
            </div>
            <div class="form-group relative-w">
               
              <?php echo $form->textField($model,'email', array('class' => 'form-control', 'placeholder'=> 'E-mail', 'type' => 'email')); ?>
              <?php echo $form->error($model,'email'); ?>     
              <i class="fa fa-envelope input-abs-icon"></i>
            </div>
            <div class="form-group relative-w">              
              <?php echo $form->passwordField($model,'password', array('class' => 'form-control', 'placeholder'=> 'Wachtwoord', 'type' => 'password')); ?>
              <?php echo $form->error($model,'password'); ?> 
              <i class="fa fa-lock input-abs-icon"></i>
            </div>
            <div class="form-group relative-w">              
              <?php echo $form->passwordField($model,'password_repeat', array('class' => 'form-control', 'placeholder'=> 'Herhaal wachtwoord', 'type' => 'password')); ?>
              <?php echo $form->error($model,'password_repeat'); ?> 
              <i class="fa fa-lock input-abs-icon"></i>
            </div>
            <br class="clear">
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Ik ga akkoord met de <a href="./register_files/register.htm">Algemene Voorwaarden</a> en onze <a href="./register_files/register.htm">Privacy Policy</a>.
                </label>
              </div>
            </div>
            <?php echo CHtml::submitButton("Account aanmaken!", array('class' => 'btn btn-success btn-rounded btn-iconed')); ?>
            <div class="no-account-yet">
              Heeft u al een account? <?php echo CHtml::link('Nu inloggen!', array('users/login'))?>
            </div>
         <?php $this->endWidget(); ?>
         
         <br class="clear clreafix">