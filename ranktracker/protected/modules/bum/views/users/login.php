<?php 
	$baseUrl = Yii::app()->theme->baseUrl;
?>		
		<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>	
             
            <?php $this->widget('facebook_app', array(
                'appId'=>Yii::app()->getModule('bum')->fb_appId,
                'secret'=>Yii::app()->getModule('bum')->fb_secret,
                'text'=>'Sign in with <b>Facebook</b>',
                'target'=>'_self',
            )); ?>
            
            <div class="lined-separator">Login via uw aangemaakte account</div>
            <div class="form-group relative-w">
              <!-- <input type="email" class="form-control" placeholder="Uw e-mailadres">  --> 
              <?php echo $form->textField($model,'username', array('class' => 'form-control', 'placeholder'=> 'Uw e-mailadres', 'type' => 'email')); ?>
              <?php echo $form->error($model,'username'); ?>
              
              <i class="fa fa-user input-abs-icon"></i>
            </div>
            <div class="form-group relative-w">              
              <?php echo $form->passwordField($model,'password', array('class' => 'form-control', 'placeholder' => 'Uw wachtwoord')); ?>
              <?php echo $form->error($model,'password'); ?>
              <i class="fa fa-lock input-abs-icon"></i>
            </div>
            <br class="clear">
            <div class="form-group">
              <div class="checkbox">
                <label>                  
                  <?php echo $form->checkBox($model,'rememberMe'); ?> Herinner mij!
                </label>
              </div>
            </div>
            
            <?php echo CHtml::submitButton('Inloggen', array('class' => 'btn btn-primary btn-rounded btn-iconed')); ?>
            <div class="no-account-yet">
              Heeft u nog geen account? <?php echo CHtml::link('Maak nu uw gratis account aan!', array('users/signUp'))?>
              <br />
              <?php echo CHtml::link('Forgoten password?',array('users/passwordRecoveryWhatUser')); ?>
            </div>
          <?php $this->endWidget(); ?>