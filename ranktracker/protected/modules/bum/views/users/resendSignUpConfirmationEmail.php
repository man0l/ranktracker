<?php
/**
 * Resend the sign up confirmation email form.
 *
 * @copyright	Copyright &copy; 2012 Hardalau Claudiu 
 * @package		bum
 * @license		New BSD License 
 */

/* @var $this UsersController */
/* @var $model FindUserBy */
/* @var $form CActiveForm */
/*
?>

<h1>Reset the Confirmation Email</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'resend-sign-up-confirmation-email-resendSignUpConfirmationEmail-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <fieldset>
        <legend>Email or User Name:</legend>
        
        <?php if (Yii::app()->user->isGuest): ?>
            <div class="row">
                <?php echo $form->labelEx($model,'email_or_user_name'); ?>
                <?php echo $form->textField($model,'email_or_user_name',array('size'=>60,'maxlength'=>60)); ?>
                <?php echo $form->error($model,'email_or_user_name'); ?>
            </div>
        <?php else: ?>
            <div class="row">
                <?php echo $form->labelEx($model,'email_or_user_name'); ?>
                <?php echo $form->textField($model,'email_or_user_name',array('size'=>60,'maxlength'=>60, 'readonly'=>'readonly', 'value'=>Yii::app()->user->primaryEmail)); ?>
                <?php echo $form->error($model,'email_or_user_name'); ?>
            </div>
        <?php endif; ?>
    </fieldset>

    <fieldset>
        <legend>Are you human?</legend>
        <?php if(CCaptcha::checkRequirements()): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'verifyCode'); ?>
            <div>
            <?php $this->widget('CCaptcha'); ?>
            <?php echo $form->textField($model,'verifyCode'); ?>
            </div>
            <div class="hint">Please enter the letters as they are shown in the image above.
            <br/>Letters are not case-sensitive.</div>
            <?php echo $form->error($model,'verifyCode'); ?>
        </div>
        <?php endif; ?>
    </fieldset>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
*/?>

<?php 
	$baseUrl = Yii::app()->theme->baseUrl;
?>		
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'resend-sign-up-confirmation-email-resendSignUpConfirmationEmail-form',
			'enableAjaxValidation'=>false,
		)); ?>	
             
            
           	<?php echo $form->errorSummary($model); ?>
            	
            <div class="lined-separator">Reset the Confirmation Email</div>
            <div class="form-group relative-w">
              <!-- <input type="email" class="form-control" placeholder="Uw e-mailadres">  -->
              
              <?php if (Yii::app()->user->isGuest): ?> 
              	 <?php echo $form->textField($model,'email_or_user_name', array('class' => 'form-control', 'placeholder'=> 'e-mailadres of gebruikersnaam')); ?>
              <?php else :?>
             	 <?php echo $form->textField($model,'email_or_user_name',array('size'=>60,'maxlength'=>60, 'readonly'=>'readonly', 'value'=>Yii::app()->user->primaryEmail)); ?>
              <?php endif;?>
              
              <?php echo $form->error($model,'username'); ?>
              
              <i class="fa fa-user input-abs-icon"></i>
            </div>
           
            <br class="clear">           
            
            <?php echo CHtml::submitButton('Inloggen', array('class' => 'btn btn-primary btn-rounded btn-iconed')); ?>
            <div class="no-account-yet">
              Heeft u nog geen account? <?php echo CHtml::link('Maak nu uw gratis account aan!', array('users/signUp'))?>
              <br />
              <?php echo CHtml::link('Forgoten password?',array('users/passwordRecoveryWhatUser')); ?>
            </div>
          <?php $this->endWidget(); ?>