<?php
/* @var $this SearchEnginesController */
/* @var $model SearchEngines */

$this->breadcrumbs=array(
	'Search Engines'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List SearchEngines', 'url'=>array('index')),
	array('label'=>'Create SearchEngines', 'url'=>array('create')),
	array('label'=>'Update SearchEngines', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SearchEngines', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SearchEngines', 'url'=>array('admin')),
);
?>

<h1>View SearchEngines #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'id',
	),
)); ?>
