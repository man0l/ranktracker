<?php
/* @var $this SearchEnginesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Search Engines',
);

$this->menu=array(
	array('label'=>'Create SearchEngines', 'url'=>array('create')),
	array('label'=>'Manage SearchEngines', 'url'=>array('admin')),
);
?>

<h1>Search Engines</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
