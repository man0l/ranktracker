<?php
/* @var $this SearchEnginesController */
/* @var $model SearchEngines */

$this->breadcrumbs=array(
	'Search Engines'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SearchEngines', 'url'=>array('index')),
	array('label'=>'Manage SearchEngines', 'url'=>array('admin')),
);
?>

<h1>Create SearchEngines</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>