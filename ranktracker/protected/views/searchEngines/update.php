<?php
/* @var $this SearchEnginesController */
/* @var $model SearchEngines */

$this->breadcrumbs=array(
	'Search Engines'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SearchEngines', 'url'=>array('index')),
	array('label'=>'Create SearchEngines', 'url'=>array('create')),
	array('label'=>'View SearchEngines', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SearchEngines', 'url'=>array('admin')),
);
?>

<h1>Update SearchEngines <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>