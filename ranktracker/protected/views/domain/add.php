<?php
/* @var $this DomainController */
 
$this->breadcrumbs=array(
	'Domain'=>array('/domain/list'),
	'Add',
);
?>


 <div class="row">
      <div class="col-md-6">

        <div class="widget widget-green">
          <span class="offset_anchor" id="anchor_nieuw_domein"></span>
          <div class="widget-title">
            <div class="widget-controls">

</div>
            <h3><i class="fa fa-check"></i> Nieuw domein toevoegen</h3>
          </div>
          <div class="widget-content">
            
              
              <?php $form = $this->beginWidget('CActiveForm', array(
				    				    
              		'htmlOptions' => array(
            	 		'class' => 	'form-horizontal'	
        	      )
				));
	 
              ?>
              <?php foreach(Yii::app()->user->getFlashes() as $key => $message) : ?>
				<div class="alert alert-success"><?php echo $message?></div>
				<?php endforeach?>
				<?php if($errorFlag) :?>
					<?php //echo $form->errorSummary($model); ?>
					<div class="alert alert-danger">
					<?php foreach($errors as $error) :?>
						<?php foreach($error as $e) :?>
						<div><?php echo $e?></div>
						<?php endforeach;?>
					<?php endforeach;?>
					</div>
				<?php endif?>
              
              
              <div class="form-group">
                <label class="col-md-4 control-label">Domeinnaam</label>
                <div class="col-md-8">
                 
                  <?php echo $form->textField($model, 'domain', array('placeholder' => 'http://www.domein.com', 'class' => 'form-control'))?>
                  <span class="help-block">Gelieve het domein met http:// ingeven!</span>
                </div>
              </div>
                <div class="form-group">
                <label class="col-md-4 control-label">Zoekwoorden</label>
                  <div class="col-md-8">                    
                   
                    <?php echo $form->textArea($model, 'keywords', array('placeholder' => 'Zoekwoorden', 'rows'=> 3, 'class' => 'form-control'))?>
                    <span class="help-block">1 Zoek woord/zin per lijn!</span>
                  </div>
                </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Zoekmachine</label>
                <div class="col-md-8">
                  <select class="form-control chosen-select" multiple="" name="AddProjectForm[searchEngine][]">
                    <option value="3">Google België (.be)</option>
                    <option value="2">Google Nederland (.nl)</option>
                    <option value="1">Google Internationaal (.com)</option>
                    <option value="6">Bing België (.be)</option>
                    <option value="5">Bing Nederland (.nl)</option>
                    <option value="4">Bing Internationaal (.com)</option>
                  </select>                  
                  <span class="help-block">U kunt meerdere zoekmachines toevoegen!</span>                  
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Statistieken kleur</label>                  
                  <div class="col-md-8">
                  <input type="text" placeholder="#FFFFFF" name="AddProjectForm[colorPick]" class="form-control input-colorpicker-simple">
                   <span class="help-block">Geef uw domein een kleur, dit is later handig voor de statistieken!</span>
                </div>
                </div>

              <div class="form-group">
                <div class="col-md-offset-4 col-md-8">
                  <button type="submit" class="btn btn-primary">Domein Toevoegen!</button>
                </div>
              </div>
          
          <?php $this->endWidget()?>
          
          </div>
        </div>
      </div>
    </div>
           