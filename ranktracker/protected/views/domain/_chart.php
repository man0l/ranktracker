<script type="text/javascript">
$(function(){
	var morris_area_options = {
	  element: "linechart_<?php echo $iterator?>",
	  behaveLikeLine: true,
	  data: [
	  <?php $i = 0; foreach($dates as $key=>$date) :?>
	    {
	      x: "<?php echo $key?>",
	      <?php foreach($date as $pos) :?>
	      'search_<?php echo $pos->search_engine->id?>': <?php echo $pos->position?>,
	   	  <?php endforeach?>
	      
	    }<?php if(sizeof($dates) - 1 !== $i ) :?>,<?php endif?>
	 <?php $i++; endforeach;?>
	    
	  ],
	  xkey: "x",
	  ykeys: [ 
			// foreach date
			"search_<?php echo $dates[$key][0]->search_engine->id ?>",
			 
			 
	  ],
	  labels: [
			// foreach search engine
			"<?php echo $dates[$key][0]->search_engine->name ?>"
	  ],
	  lineColors: ["<?php if($domain->colorPick) : echo $domain->colorPick; else : ?>#3fa1e8<?php endif?>"]
	};

	Morris.Area(morris_area_options);
	});	
</script>
 