<?php
/* @var $this DomainController */

$baseUrl = Yii::app()->theme->baseUrl;
?> 

<ol class="breadcrumb" id="anchor_inleiding">
  <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Home</a></li>
  <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Dashboard</a></li>
  <li><?php //echo $keyword->keyword?></li>
  
</ol> 

 <div class="alert alert-info alert-dismissable">
              <i class="fa fa-lightbulb"></i> <strong>Domeinen overzicht!</strong> <br><br>
              Op deze pagina heeft u een overzicht van alle domeinen met de gekoppelde zoekwoorden en de huidige positie van het zoekwoord. U kunt op een domein klikken voor meer informatie over dat desbetreffende domein en de gekoppelde zoekwoorden.<br><br>Veel succes!<br>Simpel SEO Team
              
            </div>
            
<div class="widget widget-red">
      <span class="offset_anchor" id="line_chart_anchor"></span>
      <div class="widget-title">
        <div class="widget-controls">
  
</div>
        <h3><i class="fa fa-bar-chart-o"></i> Statistieken voor de gekozen domeinnaam</h3>
      </div>
      <div class="widget-content">
        <div class="shadowed-bottom bottom-margin">
          <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-6 bordered">
              <div class="value-block value-bigger changed-up some-left-padding">
                <div class="value-self">
                  Domeinnaam
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-3 visible-md visible-lg bordered">
              <div class="value-block text-center">
                <div class="value-self"><?php echo $countTop10?>/<?php echo $count?></div>
                <div class="value-sub">Top 10 Posities</div>
              </div>
            </div>
            <div class="col-lg-2 bordered visible-lg">
              <div class="value-block text-center">
                <div class="value-self"><?php echo $countTop5?>/<?php echo $count?></div>
                <div class="value-sub">Top 5 Posities</div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <form class="form-inline form-period-selector" action="" method="post">
                <label class="control-label">Tijdspanne:</label><br>
                <input type="text" name="fromDate" placeholder="01/12/2013" class="form-control input-sm input-datepicker">
                <input type="text" name="toDate" placeholder="01/03/2014" class="form-control input-sm input-datepicker">
                <input type="submit" name="report" value="Go">
              </form>
            </div>
          </div>
        </div>
        <div class="padded">
          <div id="linechart" style="height: 250px;"></div>
        </div>
      </div>
    </div>
<div align="right">
            <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Wijzigen</button>
            <button type="button" class="btn btn-primary"><i class="fa fa-download"></i> PDF Exporteren</button>
            <button type="button" class="btn btn-success"><i class="fa fa-print"></i> Printen</button>
            <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Verwijderen</button>
</div>
<br>


<script type="text/javascript">
<?php /*?>
$(function(){
var morris_area_options = {
  element: "linechart",
  behaveLikeLine: true,
  data: [
  <?php $i = 0; foreach($results as $result) :?>
    {
      x: "<?php echo date("Y-m-d",strtotime($result->date_at))?>",
      y: <?php echo $result->position?>,
      
    }<?php if(sizeof($results) - 1 !== $i ) :?>,<?php endif?>
 <?php $i++; endforeach;?>
    
  ],
  xkey: "x",
  ykeys: ["y"],
  labels: ["Position"],
  lineColors: ["#E8BEBE", "#BCCBDD", "#3498db", "#2c3e50", "#1abc9c", "#34495e", "#9b59b6", "#e74c3c"]
};

Morris.Area(morris_area_options);
});
*/?>

$(function(){
	var morris_area_options = {
	  element: "linechart",
	  behaveLikeLine: true,
	  data: [
	  <?php $i = 0; foreach($dates as $key=>$date) :?>
	    {
	      x: "<?php echo $key?>",
	      <?php foreach($date as $k=>$d) :?>
	      <?php echo $k?>: <?php echo $d?>,
	   	  <?php endforeach?>
	      
	    }<?php if(sizeof($dates) - 1 !== $i ) :?>,<?php endif?>
	 <?php $i++; endforeach;?>
	    
	  ],
	  xkey: "x",
	  ykeys: [ "<?php echo implode("\",\"", array_keys($date))?>"],
	  labels: ["<?php echo implode("\",\"", $labels[$key])?>"],
	  lineColors: ["#E8BEBE", "#BCCBDD", "#3498db", "#2c3e50", "#1abc9c", "#34495e", "#9b59b6", "#e74c3c"]
	};

	Morris.Area(morris_area_options);
	});

</script>


<div class="row">
    <div class="widget widget-blue">
      <span class="offset_anchor" id="anchor_domein_overzicht"></span>
      <div class="widget-title">
        <div class="widget-controls">
</div>
        <h3><i class="fa fa-globe"></i> Gedetailleerd informatie domeinnaam</h3>
      </div>
      <div class="widget-content">
        <div class="table-responsive">
        
        
        <table class="table table-hover">
              <thead>
                <tr>
                  
                  <th>Zoekwoord</th>
                  <!-- 
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/google.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/be.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/nl.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/bing.png" alt=""> Bing</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/be.png" alt=""> Bing</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/nl.png" alt=""> Bing</center></th>
                  -->
                   <?php foreach($allSearchEngines as $searchEngine) :?>
                   	 <?php switch ($searchEngine->name) {
			                  case 'google.nl':
			                  		$image = "nl.png";
			                  	break;
			                  case 'google.com':
			                  		$image = "google.png";
			                  	break;
			                  case 'google.be':
			                  		$image = 'be.png';
			                  	break;
			                  case 'bing.nl':
			                  		$image = 'nl.png';
			                  	break;
			                  case 'bing.be':
			                  	$image = 'be.png';
			                  	break;
			                  case 'bing.com':
			                  	$image = 'bing.png';
			                  	break;
			                  default: $image = 'google.png';
					}?>
				   <th><center><img src="<?php echo $baseUrl?>/assets/images/<?php echo $image?>" alt=""> <?php echo ucfirst($searchEngine->name)?></center></th>
                   <?php endforeach?>
                   
                </tr>
                <tr>
                	<th></th> 
                	<?php foreach($allSearchEngines as $searchEngine) :?>
                	 <th><center>H - G - W</center></th>
                	 <?php endforeach;?>
                </tr>
              </thead>
              <tbody>
              <?php
		       $showDomain = array();
               foreach($keywords as $keyword) :
              	
              ?>
                <tr>
                  
                  <td><?php echo $keyword->keyword?></td>
                  <?php foreach($allSearchEngines as $searchEngine) :?>
                  	  <td>
	                  <?php foreach($keyword->results as $position) :?>
	                  	<?php if($searchEngine->id == $position->search_engine->id) :
								if($position->position < $position->position_week)
									$color = "red";
								elseif($position->position > $position->position_week)
								    $color = "green";
								else $color = "black";
								$color = "black";
								?>
								
	                  	<center>
	                  			<font color="<?php echo $color?>">
	                  	<?php if($position->is_parsed && $position->position > 0) :?>
	                  							<?php echo $position->position; ?> -
	                  							<?php echo $position->position_week; ?> -
	                  							<?php echo $position->position_month; ?>
										                  							
	                  							<?php elseif(!$position->is_parsed) :?><i class="fa fa-clock-o"></i>
	                  							<?php else :?>> 100
	                  							<?php endif;?>
	                  							 
	                  	 					
	                  	<?php endif?>
	                  			</font>
						</center>	
	                  	
	                  <?php endforeach;?> <!--  end keyword->results -->
	                  </td>
	              <?php  endforeach;?>
                  
                 
                </tr>
              <?php endforeach;?>
               
              </tbody>
            </table>
         
            <br><br><center>
            <b>H</b> : Huidige positie van het zoekwoord<br>
            <b>G</b> : Positie die het zoekwoord gisteren had<br>
            <b>W</b> : Gemiddelde positie van het zoekwoord over tijdspanne van 7 dagen
          </center>
            </div>

          
        
      </div>
  </div>
