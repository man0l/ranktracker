
<div class="row">
    <div class="widget widget-blue">
      <span class="offset_anchor" id="anchor_domein_overzicht"></span>
      <div class="widget-title">
        <div class="widget-controls">
</div>
        <h3><i class="fa fa-globe"></i> Gedetailleerd informatie domeinnaam</h3>
      </div>
      <div class="widget-content">
        <div class="table-responsive">
        
        
        <table class="table table-hover">
              <thead>
                <tr>
                  
                  <th>Zoekwoord</th>
                  <!-- 
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/google.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/be.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/nl.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/bing.png" alt=""> Bing</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/be.png" alt=""> Bing</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/nl.png" alt=""> Bing</center></th>
                  -->
                   <?php foreach($allSearchEngines as $searchEngine) :?>
                   	 <?php switch ($searchEngine->name) {
			                  case 'google.nl':
			                  		$image = "nl.png";
			                  	break;
			                  case 'google.com':
			                  		$image = "google.png";
			                  	break;
			                  case 'google.be':
			                  		$image = 'be.png';
			                  	break;
			                  case 'bing.nl':
			                  		$image = 'nl.png';
			                  	break;
			                  case 'bing.be':
			                  	$image = 'be.png';
			                  	break;
			                  case 'bing.com':
			                  	$image = 'bing.png';
			                  	break;
			                  default: $image = 'google.png';
					}?>
				   <th><center><img src="<?php echo $baseUrl?>/assets/images/<?php echo $image?>" alt=""> <?php echo ucfirst($searchEngine->name)?></center></th>
                   <?php endforeach?>
                   
                </tr>
                <tr>
                	<th></th> 
                	<?php foreach($allSearchEngines as $searchEngine) :?>
                	 <th><center>H - G - W</center></th>
                	 <?php endforeach;?>
                </tr>
              </thead>
              <tbody>
              <?php
		       $showDomain = array();
               foreach($keywords as $keyword) :
              	
              ?>
                <tr>
                  
                  <td><?php echo $keyword->keyword?></td>
                  <?php foreach($allSearchEngines as $searchEngine) :?>
                  	  <td>
	                  <?php foreach($keyword->results as $position) :?>
	                  	<?php if($searchEngine->id == $position->search_engine->id) :
								if($position->position < $position->position_week)
									$color = "red";
								elseif($position->position > $position->position_week)
								    $color = "green";
								else $color = "black";
								$color = "black";
								?>
								
	                  	<center>
	                  			<font color="<?php echo $color?>">
	                  	<?php if($position->is_parsed && $position->position > 0) :?>
	                  							<?php echo $position->position; ?> -
	                  							<?php echo $position->position_week; ?> -
	                  							<?php echo $position->position_month; ?>
										                  							
	                  							<?php elseif(!$position->is_parsed) :?><i class="fa fa-clock-o"></i>
	                  							<?php else :?>> 100
	                  							<?php endif;?>
	                  							 
	                  	 					
	                  	<?php endif?>
	                  			</font>
						</center>	
	                  	
	                  <?php endforeach;?> <!--  end keyword->results -->
	                  </td>
	              <?php  endforeach;?>
                  
                 
                </tr>
              <?php endforeach;?>
               
              </tbody>
            </table>
         
            <br><br><center>
            <b>H</b> : Huidige positie van het zoekwoord<br>
            <b>G</b> : Positie die het zoekwoord gisteren had<br>
            <b>W</b> : Gemiddelde positie van het zoekwoord over tijdspanne van 7 dagen
          </center>
            </div>

          
        
      </div>
  </div>