<?php
/* @var $this DomainController */

$this->breadcrumbs=array(
	'Domain'=>array('/domain/list'),
	'List',
);
?>

<div class="page-header">
	<h1> Your rankings:</h1>
</div>
 

<div class="row-fluid">
  <div class="span6"> 

<?php  
	 /*	$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>"Your ranking",
		));
		
	?>
  	<?php $this->widget('zii.widgets.grid.CGridView', array(
			'type'=>'striped bordered condensed',
			'itemsCssClass'=>'table table-hover',
			'dataProvider'=>$dataProvider,
			'template'=>"{items}",
			'columns'=>array(
				array('name'=>'id', 'header'=>'#'),
				array('name'=>'keyword', 'header'=>'Keyword'),
				//array('name'=>'domain', 'header'=>'Domain'),
				array('header' => "Domain", 'type' => 'raw', 'value' => '$data->domain->name'),
				array('header' => "Search Engine", 'type' => 'raw', 'value' => '$data->search_engine->name'),								
			),
		));  ?>
<?php $this->endWidget();  ?>
<?php ?>
		<div class="portlet" id="yw0">
				<div class="portlet-decoration">
					<div class="portlet-title">Your ranking</div>
				</div>
				<div class="portlet-content">
				  	<div id="yw1" class="grid-view">
				<table class="table table-hover">
						 
							<thead>	
								<tr>
									<th># </th>
									<th>Domain</th>
									<th>Keyword</th>
									<th>Search Engine</th>
									<th>Position</th>
									<th>Position 1 week</th>
									<!-- <th>Position 1 month</th> -->
								</tr>
							</thead>
							<tbody>
							<?php foreach($positions as $position) :?>
								 
								<tr>	
								<td><?php echo $position->id?></td>
										<td><?php echo $position->domain->name?></td>
										<td><?php echo $position->keywords->keyword?></td>
										<td><?php echo $position->search_engine->name?></td>
										<td><?php echo $position->position?></td>
										<td><?php echo $position->position_week?></td>
										<!-- <td><?php echo $position->position_month?></td>  -->
									
								</tr>
								 
							<?php endforeach;?>	
							</tbody>
						</table>
					  </div>
				</div>
		</div>	  
	  
	</div>
</div>
*/ ?>
