<?php
/* @var $this DomainController */

$baseUrl = Yii::app()->theme->baseUrl;
?> 

<ol class="breadcrumb" id="anchor_inleiding">
  <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Home</a></li>
  <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Dashboard</a></li>
  <li class="active">OVERZICHT</li>
</ol> 

   <div class="alert alert-info alert-dismissable">
              <i class="fa fa-lightbulb"></i> <strong>Domeinen overzicht!</strong> <br><br>
              Op deze pagina heeft u een overzicht van alle domeinen met de gekoppelde zoekwoorden en de huidige positie van het zoekwoord. U kunt op een domein klikken voor meer informatie over dat desbetreffende domein en de gekoppelde zoekwoorden.<br><br>Veel succes!<br>Simpel SEO Team
              
            </div>

<div class="row">
    <div class="widget widget-blue">
      <span class="offset_anchor" id="anchor_domein_overzicht"></span>
      <div class="widget-title">
        <div class="widget-controls">
		</div>
        <h3><i class="fa fa-globe"></i> Domeinen overzicht</h3>
      </div>
      <div class="widget-content">
            
<div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Domeinnaam</th>
                  <th>Zoekwoord</th>
                  <!-- 
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/google.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/be.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/nl.png" alt=""> Google</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/bing.png" alt=""> Bing</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/be.png" alt=""> Bing</center></th>
                  <th><center><img src="<?php echo $baseUrl?>/assets/images/nl.png" alt=""> Bing</center></th>
                  -->
                   <?php foreach($allSearchEngines as $searchEngine) :?>
                   	 <?php switch ($searchEngine->name) {
			                  case 'google.nl':
			                  		$image = "nl.png";
			                  	break;
			                  case 'google.com':
			                  		$image = "google.png";
			                  	break;
			                  case 'google.be':
			                  		$image = 'be.png';
			                  	break;
			                  case 'bing.nl':
			                  		$image = 'nl.png';
			                  	break;
			                  case 'bing.be':
			                  	$image = 'be.png';
			                  	break;
			                  case 'bing.com':
			                  	$image = 'bing.png';
			                  	break;
			                  default: $image = 'google.png';
					}?>
				   <th><center><img src="<?php echo $baseUrl?>/assets/images/<?php echo $image?>" alt=""> <?php echo ucfirst($searchEngine->name)?></center></th>
                   <?php endforeach?>
                  <th>Acties</th>
                </tr>
              </thead>
              <tbody>
              <?php
		       $showDomain = array();
               foreach($keywords as $keyword) :
              	
              ?>
                <tr>
                  <td><?php if(!in_array($keyword->domain->name, $showDomain)) echo $keyword->domain->name?></td>
                  <td><?php echo $keyword->keyword?></td>
                  <?php foreach($allSearchEngines as $searchEngine) :?>
                  	  <td>
	                  <?php foreach($keyword->results as $position) :?>
	                  	<?php if($searchEngine->id == $position->search_engine->id) :
								if(($position->position < $position->position_week) && $position->position_week != 0 )
									$color = "#0EF029";
								elseif(($position->position > $position->position_week) && $position->position_week != 0)								
								    $color = "red";
								elseif($position->position > 0 && $position->position_week == 0)
									$color = "#0EF029";
								else $color = "black";
						?>
	                  	<center>
	                  			<font color="<?php echo $color?>" style="font-weight: bold">
	                  	<?php if($position->is_parsed && $position->position > 0) :?>
	                  			
	                  							<?php echo $position->position; ?>
										                  							
	                  		  <?php elseif(!$position->is_parsed) :?>
	                  		  	<?php echo $position->position?>
	                  		  	<i class="fa fa-clock-o"></i>
	                  		  <?php else :?>> 100
	                    	  <?php endif;?>
	                  							 
	                  	 					
	                  	<?php endif?>
	                  			</font>
						</center>	
	                  	
	                  <?php endforeach;?> <!--  end keyword->results -->
	                  </td>
	              <?php  endforeach;?>
                  
                  <td>    
                   
                  	<i class="fa fa-search"></i>              	
                  	<?php echo CHtml::link('Bekijk in detail!', array('domain/details', 'id' => $keyword->id))?>
                   
                  </td>
                </tr>
              <?php $showDomain[] = $keyword->domain->name; endforeach;?>
               
              </tbody>
            </table>
            </div>

        </div>
    </div>
</div>