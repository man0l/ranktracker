<?php
/* @var $this DomainController */

$baseUrl = Yii::app()->theme->baseUrl;
?> 

<ol class="breadcrumb" id="anchor_inleiding">
  <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Home</a></li>
  <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Dashboard</a></li>
  <li><?php //echo $keyword->keyword?></li>
  
</ol> 

 <div class="alert alert-info alert-dismissable">
              <i class="fa fa-lightbulb"></i> <strong>Domeinen overzicht!</strong> <br><br>
              Op deze pagina heeft u een overzicht van alle domeinen met de gekoppelde zoekwoorden en de huidige positie van het zoekwoord. U kunt op een domein klikken voor meer informatie over dat desbetreffende domein en de gekoppelde zoekwoorden.<br><br>Veel succes!<br>Simpel SEO Team
              
 </div>
 
<!--  chart  -->
 <div class="widget widget-red">
      <span class="offset_anchor" id="line_chart_anchor"></span>
      <div class="widget-title">
        <div class="widget-controls">
  
</div>
        <h3><i class="fa fa-bar-chart-o"></i> Statistieken voor de gekozen domeinnaam</h3>
      </div>
      <div class="widget-content">
        <div class="shadowed-bottom bottom-margin">
          <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-6 bordered">
              <div class="value-block value-bigger changed-up some-left-padding">
                <div class="value-self">
                  <?php echo $keyword->keyword?>
                </div>
              </div>
            </div>
            <!-- 
            <div class="col-lg-2 col-md-3 visible-md visible-lg bordered">
              <div class="value-block text-center">
                <div class="value-self"><?php echo $countTop10?>/<?php echo $count?></div>
                <div class="value-sub">Top 10 Posities</div>
              </div>
            </div>
            <div class="col-lg-2 bordered visible-lg">
              <div class="value-block text-center">
                <div class="value-self"><?php echo $countTop5?>/<?php echo $count?></div>
                <div class="value-sub">Top 5 Posities</div>
              </div>
            </div>
             -->
            <div class="col-lg-4 col-md-4 col-sm-6">
              <form class="form-inline form-period-selector" action="" method="post">
                <label class="control-label">Tijdspanne:</label><br>
                <input type="text" name="fromDate" placeholder="01/12/2013" class="form-control input-sm input-datepicker">
                <input type="text" name="toDate" placeholder="01/03/2014" class="form-control input-sm input-datepicker">
                <input type="submit" name="report" value="Go">
              </form>
            </div>
          </div>
        </div>
        <div class="padded">
          <?php for($i = 0; $i < $countRows; $i++) : ?>
          	<div id="linechart_<?php echo $i?>" style="height: 250px;"></div>
          <?php endfor?>
        </div>
      </div>
    </div>
	<div align="right">
	            <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Wijzigen</button>
	            <button type="button" class="btn btn-primary"><i class="fa fa-download"></i> PDF Exporteren</button>
	            <button type="button" class="btn btn-success"><i class="fa fa-print"></i> Printen</button>
	            <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Verwijderen</button>
	</div>
<!--  end chart  -->

<?php $i = 0; foreach($results as $result) :?>
	<?php echo $this->renderPartial("_chart", array('dates' => $result, 'iterator' => $i, 'domain'=>$keyword->domain))?>
<?php $i++; endforeach;?>

<br>

<?php $this->renderPartial("_table_history", array('baseUrl' => $baseUrl, 'allSearchEngines' => $allSearchEngines, 'keywords' => $keywords))?>

