<?php
/*
 ?>

<div class="page-header">
<h1> Add your domain and keywords in the wizzard:</h1>
</div>


<div class="row-fluid">
<div class="span6">

<?php foreach(Yii::app()->user->getFlashes() as $key => $message) : ?>
<div class="alert alert-success"><?php echo $message?></div>
<?php endforeach?>

<?php echo(CHtml::beginForm(array("domain/add"), "post", array('id'=>'wizzard_form'))); ?>
<div id="wizzard">
<h3>Domain</h3>
<section>
<div class="clearfix">
<?php echo(CHtml::label('Your domain:', 'domain'));?>

<?php echo(CHtml::textField('domain','', array('placeholder'=> 'example.com'))); ?>
</div>
<div class="clearfix">
<?php echo(CHtml::label('Search Engine:', 'search_engine'));?> <br />
<?php //echo(CHtml::dropDownList('search_engine','Google.be', array('google.be' => "Google.be", 'google.nl' => 'Google.nl', "bing.be" => 'Bing.be', 'bing.nl' => 'Bing.nl'))); ?>
<?php //echo(CHtml::activeDropDownList($searchEngines, 'name', CHtml::listData($searchEngines, 'id', 'name') )); ?>
<?php echo (CHtml::checkBoxList('searchEngines', array(), CHtml::listData($searchEngines, 'id', 'name')))?>
</div>

</section>
<h3>Keywords</h3>
<section>
	
<?php echo(CHtml::label('Keywords', 'keywords'));?>

<div class="clearfix"><?php echo(CHtml::textArea('keyword','',array('placeholder' =>"your keywords", 'rows'=> 15, 'cols'=> 100))); ?>
<br class="clear" />
<small class="clearfix">one keyword on every row</small>
</div>
</section>
</div>
<?php echo(CHtml::endForm());?>



</div>
</div>

<script type="text/javascript">
$("#wizzard").steps({
		bodyTag: "section",
		headerTag: "h3",
		onFinished: function(event, currentIndex) {
		$("#wizzard_form").submit();
		},

		onStepChanging: function(event, currentIndex, newIndex) {
		var error = true;

		var domain = $('#domain');
		var keywords = $("#keyword");

		if(currentIndex == 0) {
			
			
		if(!domain.val()) {
		error = false;
		domain.css({ borderColor: "#ff3111"});
		}
			

		if($("input[name='searchEngines[]']:checked").length == 0) {
		error = false;
		//$("input[name='searchEngines[]']").css( { borderColor: "#ff3111"} );
		$("label[for^=searchEngines]").css( { color: "#ff3111"} );
		} else {
		$("label[for^=searchEngines]").css( { color: "#000"} );
		}

		} else if(currentIndex === 1) {
			

		if(!keywords.val()) {
		error = false;
		keywords.css({ borderColor: "#ff3111"});
		}
		}


		return error;
		},
		onStepChanged: function(event, currentIndex, priorIndex) {
		$('#domain').css({ border: "1px solid #ccc" });
		}
		});

</script>
*/ ?>