<?php

/**
 * This is the model class for table "results".
 *
 * The followings are the available columns in table 'results':
 * @property integer $domain_id
 * @property integer $search_engine_id
 * @property integer $keywords_id
 * @property integer $position
 * @property string $date_at
 * @property integer $id
 * @property integer $user_id
 */
class PositionResults extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'results';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('domain_id, search_engine_id, keywords_id, position, user_id', 'required'),
			array('domain_id, search_engine_id, keywords_id, position, user_id', 'numerical', 'integerOnly'=>true),
			array('date_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('domain_id, search_engine_id, keywords_id, position, date_at, id, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'domain_id' => 'Domain',
			'search_engine_id' => 'Search Engine',
			'keywords_id' => 'Keywords',
			'position' => 'Position',
			'date_at' => 'Date At',
			'id' => 'ID',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('domain_id',$this->domain_id);
		$criteria->compare('search_engine_id',$this->search_engine_id);
		$criteria->compare('keywords_id',$this->keywords_id);
		$criteria->compare('position',$this->position);
		$criteria->compare('date_at',$this->date_at,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PositionResults the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
