<?php

/**
 * This is the model class for table "results".
 *
 * The followings are the available columns in table 'results':
 * @property integer $domain_id
 * @property integer $search_engine_id
 * @property integer $keywords_id
 * @property integer $position
 * @property integer $position_week
 * @property integer $position_month
 * @property integer $is_parsed
 * @property integer $is_exported
 * @property string $date_at
 * @property integer $id
 * @property integer $user_id
 */
class Positions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'results';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('domain_id, search_engine_id, keywords_id, position, position_week, position_month, is_exported, user_id', 'required'),
			array('domain_id, search_engine_id, keywords_id, position, position_week, position_month, is_parsed, is_exported, user_id', 'numerical', 'integerOnly'=>true),
			array('date_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('domain_id, search_engine_id, keywords_id, position, position_week, position_month, is_parsed, is_exported, date_at, id, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
				'keywords' => array(self::BELONGS_TO, 'Keyword', 'keywords_id'),
				'domain' => array(self::BELONGS_TO, 'Domain', 'domain_id'),
				'search_engine'=> array(self::BELONGS_TO, 'SearchEngines', 'search_engine_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'domain_id' => 'Domain',
			'search_engine_id' => 'Search Engine',
			'keywords_id' => 'Keywords',
			'position' => 'Position',
			'position_week' => 'Position Week',
			'position_month' => 'Position Month',
			'is_parsed' => 'Is Parsed',
			'is_exported' => 'Is Exported',
			'date_at' => 'Date At',
			'id' => 'ID',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('domain_id',$this->domain_id);
		$criteria->compare('search_engine_id',$this->search_engine_id);
		$criteria->compare('keywords_id',$this->keywords_id);
		$criteria->compare('position',$this->position);
		$criteria->compare('position_week',$this->position_week);
		$criteria->compare('position_month',$this->position_month);
		$criteria->compare('is_parsed',$this->is_parsed);
		$criteria->compare('is_exported',$this->is_exported);
		$criteria->compare('date_at',$this->date_at,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Positions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
