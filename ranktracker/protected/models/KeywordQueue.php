<?php

/**
 * This is the model class for table "keyword_queue".
 *
 * The followings are the available columns in table 'keyword_queue':
 * @property integer $keyword_id
 * @property string $keyword
 * @property integer $position
 * @property integer $result_id
 * @property integer $is_parsed
 * @property integer $id
 */
class KeywordQueue extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'keyword_queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('keyword_id, keyword, position, result_id, is_parsed', 'required'),
			array('keyword_id, position, result_id, is_parsed', 'numerical', 'integerOnly'=>true),
			array('keyword', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('keyword_id, keyword, position, result_id, is_parsed, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'mappedKeyword' => array(self::BELONGS_TO, 'Keyword', 'keyword_id'),
				'result' => array(self::BELONGS_TO, 'Positions', 'result_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'keyword_id' => 'Keyword',
			'keyword' => 'Keyword',
			'position' => 'Position',
			'result_id' => 'Result',
			'is_parsed' => 'Is Parsed',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('keyword_id',$this->keyword_id);
		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('result_id',$this->result_id);
		$criteria->compare('is_parsed',$this->is_parsed);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KeywordQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
