<?php

class AddProjectForm  extends CFormModel {
	
	public $domain;
	public $keywords;
	public $searchEngine;
	public $pickColor;
	
	
	public function rules()
	{
		return array(
			array('domain, keywords, searchEngine', 'required'),
			array('domain', 'url', 'defaultScheme' => 'http') 
				
		);
	}
	
	/* public function attributeLabels()
	{
		return array(
			'domain' => 'Domeinnaam',
			'keywordz' => 'Zoekwoorden',
			'search_engine' => 'Zoekmachine',
			'pickColor'	=> 'Statistieken kleur'
				
		);
	} */
	
} 