<?php

/**
 * This is the model class for table "proxies".
 *
 * The followings are the available columns in table 'proxies':
 * @property string $proxy
 * @property string $ip_address
 * @property integer $port
 * @property integer $is_valid
 * @property integer $is_banned
 * @property integer $proxy_type
 * @property integer $valid_retries
 * @property string $created_at
 * @property integer $id
 */
class Proxies extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proxies';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('proxy, ip_address, port, is_valid, is_banned, proxy_type, valid_retries, created_at', 'required'),
			array('port, is_valid, is_banned, proxy_type, valid_retries', 'numerical', 'integerOnly'=>true),
			array('proxy, ip_address', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('proxy, ip_address, port, is_valid, is_banned, proxy_type, valid_retries, created_at, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'proxy' => 'Proxy',
			'ip_address' => 'Ip Address',
			'port' => 'Port',
			'is_valid' => 'Is Valid',
			'is_banned' => 'Is Banned',
			'proxy_type' => 'Proxy Type',
			'valid_retries' => 'Valid Retries',
			'created_at' => 'Created At',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('proxy',$this->proxy,true);
		$criteria->compare('ip_address',$this->ip_address,true);
		$criteria->compare('port',$this->port);
		$criteria->compare('is_valid',$this->is_valid);
		$criteria->compare('is_banned',$this->is_banned);
		$criteria->compare('proxy_type',$this->proxy_type);
		$criteria->compare('valid_retries',$this->valid_retries);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proxies the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
