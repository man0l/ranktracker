<?php

/**
 * This is the model class for table "search_engines_domain".
 *
 * The followings are the available columns in table 'search_engines_domain':
 * @property integer $search_engines_id
 * @property integer $domain_id
 * @property integer $id
 */
class SearchEnginesDomain extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'search_engines_domain';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('search_engines_id, domain_id', 'required'),
			array('search_engines_id, domain_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('search_engines_id, domain_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				//'domains' => array(self::MANY_MANY, 'Domain', 'search_engines_domain(search_engines_id, domain_id)' ),
				'domain' => array(self::BELONGS_TO, 'Domain', 'domain_id'),
				'searchEngine'=> array(self::HAS_ONE, 'SearchEngines', 'search_engines_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'search_engines_id' => 'Search Engines',
			'domain_id' => 'Domain',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('search_engines_id',$this->search_engines_id);
		$criteria->compare('domain_id',$this->domain_id);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SearchEnginesDomain the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
