<?php

require_once( dirname(__FILE__) . '/../components/SimpleHTMLDom.php');

abstract class Crawler {
		
		protected  $keyword;
		protected  $searchEngine;
		protected  $searchEngineClean;
		protected  $result;
		protected  $cssSelector;
		protected  $domain;
		protected  $domainClean;
		protected  $position = 0;
		protected  $start = 0;
 		protected  $iterator = 1;
 		protected  $notInTop100 = false;
 		protected  $interface;
 		protected  $crawlUrl;
		
		public function __construct(Keyword $keyword, SearchEngines $searchEngine, Domain $domain) 
		{
			 
			 $this->keyword = $keyword;
			 $this->searchEngine = $searchEngine;
		 	 $this->searchEngineClean = preg_replace("/(https:\/\/|http:\/\/)(www.)?/", "", rtrim($this->searchEngine->name));		 	 
		 	 $this->domainClean = preg_replace("/(https:\/\/|http:\/\/)(www.)?/", "", $domain->name);
		 	 $this->domain = $domain;
		 	 
		 	 $this->crawlUrl = $this->buildUrl();
			 
		}

		public function setInterface($interface) 
		{
			$this->interface = $interface;
		}
		
		public function getInterface()
		{
			return $this->interface;
		}		
		
		public function getPosition() {
			
			return $this->position;
			
		}
		
		public function getStart() 
		{ 
			return $this->start; 
		}
		
		public function setStart($start) 
		{
			$this->start = $start;
			
			return $this->start;
		}
		
		public function rewindStart()
		{
			$this->start -= 10;
			return $this->start;
		}
		
		
		public function getResult() { 
			return $this->result;
		}
		
		/**
		 * create the dom tree and find the position
		 */
		abstract function domTreeFromResult();
		
		/**
		 * build url for crawiling
		 * @return string $url
		 */
		abstract function buildUrl();
		
		public function parse() 
		{
			
			if(!$this->interface)
				throw new Exception("You have to set a network interface e.g. IP address before parsing");
			
			$this->result = '';			
			
			if($this->iterator < 101) {
				$ch = curl_init($this->crawlUrl);
				
				curl_setopt ($ch, CURLOPT_HEADER, 0);
				curl_setopt ($ch, CURLOPT_FAILONERROR, 1);
				curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
				$cookie_file = "cookie1.txt";
				curl_setopt ($ch, CURLOPT_COOKIESESSION, true);
				curl_setopt ($ch, CURLOPT_COOKIEFILE, $cookie_file);
				curl_setopt ($ch, CURLOPT_COOKIEJAR, $cookie_file);
				curl_setopt($ch, CURLOPT_INTERFACE, $this->interface);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
				
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
				
					
				if(!($result = curl_exec($ch)))
				{
					$error = curl_error($ch);
					echo $error."\n\r";
					// change the interface				
					
					
				} else {
					$this->result = $result;
					 
				}
				curl_close($ch);
			} else {
				$this->notInTop100 = true;
			}
			
			$this->start += 10;
		}	
		 
		
	}
	
	
	
	
 