<?php

//require_once( dirname(__FILE__) . '/../components/SimpleHTMLDom.php');

class GoogleCrawler extends Crawler {
			
		
		function domTreeFromResult() 
		{
			$positionFound = true;
			if($this->result) {
			 
			 
			 $dom = str_get_html($this->result);
			 
			 $urls = $dom->find("h3.r a");			 
			 
			 foreach($urls as $url) {		
			 	
			 	if(preg_match("/{$this->domainClean}/", $url->attr['href'], $match)) {
			 		//print_r($match);
			 		
			 		$positionFound = false;
			 		$this->position = $this->iterator;
			 		
			 		break;
			 	}
			 	
			 	 $this->iterator++;
			 }
			 
			  
			} 

			if($this->notInTop100) {
				// break the current cycle and continue with the next keyword!
				$positionFound = false;
			}
			
			 return $positionFound;
			
		}

		function buildUrl()
		{
			$url = "http://www.".$this->searchEngineClean."/search?q=".rawurlencode($this->keyword->keyword);
			if($this->start > 0)
			{
				$url .= "&start=".$this->start;
			}
			
			$this->crawlUrl = $url;
			
			return $this->crawlUrl;
		}
		
	}
	
	
	
	
 