<?php

class BingCrawler extends Crawler {
			
		function __construct($keyword, $searchEngine, $domain) {
				
			parent::__construct($keyword, $searchEngine, $domain);
				
			 
		}
		
		function domTreeFromResult() 
		{
			$positionFound = true;
			if($this->result) {
			 
			 
			 $dom = str_get_html($this->result);
			 
			 $urls = $dom->find("div.sb_tlst h3 a");			 
			 
			 foreach($urls as $url) {		
			 	
			 	if(preg_match("/{$this->domainClean}/", $url->attr['href'], $match)) {
			 		//print_r($match);
			 		
			 		$positionFound = false;
			 		$this->position = $this->iterator;
			 		
			 		break;
			 	}
			 	
			 	 $this->iterator++;
			 }
			 
			  
			} 

			if($this->notInTop100) {
				// break the current cycle and continue with the next keyword!
				$positionFound = false;
			}
			
			 return $positionFound;
			
		}

		function buildUrl()
		{
			$url = "http://www.".$this->searchEngineClean."/search?q=".rawurlencode($this->keyword->keyword);
			if($this->start > 0)
			{
				$url .= "&first=".$this->start;
			}
			
			$this->crawlUrl = $url;
			
			return $this->crawlUrl;
		}
		
	}
	
	
	
	
 