<?php

class WeekPositionsCommand extends CConsoleCommand
{
	public function run($args)
	{
		$check = Positions::model()->find(array(				 
				'order' => 'date_at DESC',					
		));
		
		 
		if($check) {
			$date = new DateTime($check->date_at);
			
			$dateAt = $date->format("Y-m-d");
			
			$results = Positions::model()->findAll(array(
					'condition' => 'date_at > :date_at',
					'params' => array(':date_at' => $dateAt )				
			));
			
			
			if($results) 
			{
				foreach($results as $result)
				{
					$dateModify = $date->modify('-7 day');
					$sevenDays = Positions::model()->find(array(
						'condition' => 'date_at > :date_at AND date_at < :less_date AND keywords_id = :keywords_id AND search_engine_id = :search_engine_id',
						'params' => array(
									':date_at' =>  $dateModify->format("Y-m-d")." 00:00", 
									':less_date' => $dateModify->format("Y-m-d") ." 23:59",
									':keywords_id' => $result->keywords_id,
									':search_engine_id' => $result->search_engine_id
						
							)		
					));
					
					print_r($sevenDays);
					
					if(!$sevenDays)
					{
						$sevenDays = $result;
					}
					
					$result->position_month = $sevenDays->position_week;
					$result->save();
				}
			}
		}
		
	}
}