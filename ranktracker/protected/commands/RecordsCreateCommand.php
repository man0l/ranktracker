<?php
/**
 * A cron command that runs 24 hours for adding new records
 * @author Manol Trendafilov <manol.trendafilov@gmail.com>
 *
 */
class RecordsCreateCommand extends CConsoleCommand
{
	function run($args)
	{

		$lastResult = Positions::model()->find(array('order' => 'date_at DESC'));
		$date = new \DateTime($lastResult->date_at);
		
		
		$dateFormatted = $date->format("Y-m-d");
		 
		$results = Positions::model()->findAll(array('condition' =>"date_at >= :date", 'params' => array(':date' => $dateFormatted)));

		foreach($results as $result) {
				
				$check =  Positions::model()->findAll(array(
							'condition' =>"date_at = :date AND keywords_id = :keyword_id AND search_engine_id = :se_id",
						 	'params' => array(':date' => date("Y-m-d 00:00:00"), ':keyword_id' => $result->keywords_id, ':se_id' => $result->search_engine_id)
				
						));
						
				 
				
				if( !$check ) {			
					$newPosition = new Positions();
					$newPosition->date_at = date("Y-m-d 00:00:00");
					//$newPosition->date_at = $dateFormatted;
					$newPosition->domain_id = $result->domain_id;
					$newPosition->is_exported = 0;
					$newPosition->search_engine_id = $result->search_engine_id;
					$newPosition->keywords_id = $result->keywords_id;
					$newPosition->user_id = $result->user_id;
					$newPosition->position = $result->position;
					$newPosition->position_month = $result->position_month;
					$newPosition->position_week = $result->position_week;
					$newPosition->save();				 
					 
				}
				
			}
		
	} 	
}