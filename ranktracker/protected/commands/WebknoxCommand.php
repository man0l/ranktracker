<?php

class WebknoxCommand extends CConsoleCommand 
{
	
	private $proxy;
	private $proxyPort;
	private $proxies;
	
	public function run ($args) 
	{
		
		Unirest::verifyPeer(false);
		$response = Unirest::get(
				"https://webknox-proxies.p.mashape.com/proxies/newMultiple?maxResponseTime=10&batchSize=10",
				array(
						"X-Mashape-Authorization" => "cnC4mEusbWbwPePYmUJOctiJno6zRhDS"
				),
				null
		);
		
	 
		foreach($response->body as $proxy) {
				
			//$googleRepsonse = Unirest::get("https://www.google.com");
			$url = "http://www.google.com";
				
			$this->curlReq($url, $proxy->proxy);
		}
		
			
	}
	
	function curlReq($proxy) {	
		
		list($ipAddress, $port) = explode(":", $proxy);
		if($this->pingGoogle($ipAddress, $port, 0)){
			// save the proxy
			$prox = new Proxies();
			list($prox->ip_address, $prox->port) = explode(":", $proxy);
			$prox->proxy = $proxy;
			$prox->is_valid = 1;
			$prox->created_at = date("Y-m-d H:i:s");
			$prox->is_banned = 0;
			$prox->proxy_type = 0;
			$prox->valid_retries = 0;
				
			$prox->save();
		}
	}
	
	function pingGoogle($proxy, $port, $type) {
	
		$working = 0;
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, "http://google.com");
			
		curl_setopt ($ch, CURLOPT_HEADER, 0);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt ($ch, CURLOPT_PROXY, trim ($proxy));
		curl_setopt	($ch, CURLOPT_PROXYPORT, $port);
		curl_setopt ($ch, CURLOPT_PROXYTYPE, $type);
	
		$result = curl_exec($ch);
		$error = curl_error($ch);
	
		$info = curl_getinfo($ch);
	
		if($info ['http_code'] == 301)
		{
			// it works!
			$working = 1;
		}
	
		return $working;
	}
}