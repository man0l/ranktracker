<?php

class NewRecordsRemoteCommand extends CConsoleCommand 
{
	public function run($args) 
	{

		$url = "http://localhost/ranktracker/ranktracker/index.php?r=remote/export";
		$url = "http://92.48.195.16/ranktracker/ranktracker/index.php?r=remote/export";
		
		$unirest = Unirest::get($url);
		
	  
		
		foreach($unirest->body as $result) {
			
			// check for existance
		 
				$checkDomain = Domain::model()->findByAttributes(array('name' => $result->domain));
				
				if(!$checkDomain) {
					// save domain
					$domain = new Domain;
					$domain->user_id = $result->user_id;
					$domain->name = $result->domain;
					$domain->created_at = date("Y-m-d H:i:s");
					$domain->save();					
				} else {
					$domain = $checkDomain;
				}
				
				$checkKeyword = Keyword::model()->findByAttributes(array('keyword' => $result->keyword));
				
				if(!$checkKeyword) {
					$keyword = new Keyword();
					$keyword->domain_id =  $domain->id;
					$keyword->keyword = $result->keyword;
					$keyword->save();
				} else {
					$keyword = $checkKeyword;
				}

				
				$position = new Positions();
				$position->date_at = date("Y-m-d H:i:s");
				$position->domain_id = $domain->id;
				$position->is_exported = 0;
				$position->search_engine_id = $result->search_engine; 
				$position->keywords_id = $keyword->id;
				$position->user_id = $result->user_id;
				$position->position = 0;
				$position->position_month = 0;
				$position->position_week = 0;
				$position->save();		

				
			 
		}
		
	}
	
	
}