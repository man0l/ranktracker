<?php

class PositionsImportCommand extends CConsoleCommand 
{
	
	function run($args) 
	{
		
		$url = "http://localhost/ranktracker/ranktracker/index.php?r=remote/import";
		$url = "http://92.48.195.145/ranktracker/index.php?r=remote/import";
		
		$unirest = Unirest::get($url);		 
		
		foreach($unirest->body as $result) {
			 
			if($result) {
				$domain = Domain::model()->findByAttributes(array('name' => $result->domain));
				$keyword = Keyword::model()->findByAttributes(array('keyword' => $result->keyword));
									
				 if($domain && $keyword) {
					
					$check = Positions::model()->find(array(
						'condition' => "keywords_id = :keywords_id AND domain_id = :domain_id AND search_engine_id = :search_engine_id",
						'params' => array(":keywords_id" => $keyword->id, ":domain_id" => $domain->id, ":search_engine_id" => $result->search_engine),
						'order' => 'date_at DESC',
							
					));
					
				 	 
					
					if($check) {
					$date = new DateTime($check->date_at);
					 
					$dateAt = $date->format("Y-m-d");
					
				
					$position = Positions::model()->find(array(
							'condition' => "keywords_id = :keywords_id AND domain_id = :domain_id AND search_engine_id = :search_engine_id AND date_at >= :date_at",
							'params' => array(":keywords_id" => $keyword->id, ":domain_id" => $domain->id, ":search_engine_id" => $result->search_engine, ':date_at' => $dateAt),
							'order' => 'date_at DESC',
								
					));
					
					 
					 
					if($position) {
						
						$position->position_week = $position->position;
						$position->position = $result->position;	
						$position->is_parsed = $result->is_parsed;
					
						$position->save();
					 
					}
					}
				
				}	
			}
		}
		
	} 
	
}