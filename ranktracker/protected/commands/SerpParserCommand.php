<?php

class SerpParserCommand extends CConsoleCommand 
{
	function run($args) 
	{
		//$this->addQueue("");
		//if(!$this->hasQueue())
			$this->addQueue();
		
		
		
		$check = Proxies::model()->findByAttributes(array('is_banned' => 0, 'is_valid' => 1));
		
		if($check) {
			$queue = KeywordQueue::model()->findAll();
			
			foreach($queue as $q) {
			$crawler = new GoogleCrawler(
										 $q->result->keywords, 
										 $check, 
										 $q->result->search_engine, 
										 $q->result->domain);		
			 
				$positionFound = true;
				$i = 0;
				
				while($positionFound) {
					
					$crawler->parse();
					$positionFound = $crawler->domTreeFromResult();
					
					if(!$crawler->getResult()) { 
						/*
						$crawler->start -= 10;
						$check = Proxies::model()->findByAttributes(array('is_banned' => 0, 'is_valid' => 1));
						if($check) {						
							$crawler->changeProxy($check);
						} else break;
						*/
					}
					
					
					
					echo " iterator ".$crawler->getIterator();
					sleep(60);
					$i++;
 				}
				
				$q->is_parsed = 1;
				$q->position = $crawler->getPosition();
				$q->save();
				
				//print_r($q);
				
				$q->result->position = $q->position;
				$q->result->save();
				
				//print_r($q);
			}
		}
		
		// move queue to next 10 kwds
		
		
	}
	
	function hasQueue() {		
		return KeywordQueue::model()->count() > 0;
	}
	
	function addQueue() {

		// how many keywords to put in the keyword queue 
		$maxKeywords = 10;
		$count = KeywordQueue::model()->count();
		$countParsed = KeywordQueue::model()->count("is_parsed = :is_parsed", array(':is_parsed' => 1));
		 
	 	if($this->hasQueue()) {
			if($countParsed > 0 &&  $count > 0 && $count == $countParsed) {
				// delete the queue and add new keywords
				KeywordQueue::model()->deleteAll();
				
			    $keywordQueueStart = KeywordQueueStart::model()->find();
			    $keywordQueueStart->start_at += $maxKeywords;
			    $keywordQueueStart->save();
			    
			    $criteriaParams = array(
			    		'order'=> 'id ASC',
			    		'limit' => $keywordQueueStart->start_at,
			    		'offset' => $maxKeywords
			    );
			    
			    $this->addKeywords($criteriaParams);
			}		
		
	 	} else {
	 		
	 		$criteriaParams = array(
	 				'order'=> 'id ASC',
	 				'limit' => $maxKeywords,
	 				'offset' => 0
	 		);
	 		
	 		$this->addKeywords($criteriaParams);
	 		
	 		$keywordQueueStart = new KeywordQueueStart();
	 		$keywordQueueStart->start_at = 0;
	 		$keywordQueueStart->end_at = $maxKeywords;
	 		$keywordQueueStart->save();
	 	}
		
			
			 
		 
	}
	
	public function addKeywords($criteriaParams) {
		
		$criteria = new CDbCriteria($criteriaParams);
			
		//$keywords = Keyword::model()->findAll($criteria);
		$results = Positions::model()->findAll($criteria);
			
		if($results) {
			foreach($results as $result) {
					
				$queue = new KeywordQueue();
				$queue->is_parsed  = 0;
				$queue->keyword = $result->keywords->keyword;
				$queue->keyword_id = $result->keywords->id;
				$queue->result_id = $result->id;
				$queue->position = 0;
				$queue->save();
		
			}
		}
		
	}
	
	function addQueue1() {
		// if has queue
		
			// if all keywords are parsed
				// add keywods with limit n/10
			// else do nothing
		// else 
			// add keywords
		
	}
	
	function flushQueue() 
	{
		
	}
}