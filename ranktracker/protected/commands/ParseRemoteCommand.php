<?php

class ParseRemoteCommand extends CConsoleCommand
{
	
	private $interfaces = array(
			
		'92.48.195.145',
		'92.48.195.146',
		'92.48.195.147',
		'92.48.195.148',
		'92.48.195.149',
		'92.48.195.150',
		'92.48.195.151',
		'92.48.195.152',
		'92.48.195.153',
		'92.48.195.154',
		//'37.157.177.9'	
	);
	
	private $currentInterface;
	
	function run($args) {
		
		$randomInterface = mt_rand(0, sizeof($this->interfaces) - 1);
		$this->currentInterface = $this->interfaces[$randomInterface];
		
		$this->addQueue();
		$this->processQueue();
	}

	function processQueue() 
	{
		$queue = KeywordQueue::model()->findAllByAttributes(array('is_parsed' => 0));
		$keywordQueueStart = KeywordQueueStart::model()->find();
		
		if($keywordQueueStart->is_parsing)
		{
			return;
		}
			
		foreach($queue as $q) {
			$searchEngineClean = preg_replace("/(https:\/\/|http:\/\/)(www.)?/", "", rtrim($q->result->search_engine->name));
			
			switch($searchEngineClean) {
				case 'google.com':
				case 'google.nl':
				case 'google.be':
				case 'google.bg':
					
				$crawler = new GoogleCrawler(
						$q->result->keywords,					
						$q->result->search_engine,
						$q->result->domain);
				break;
				case 'bing.com':
				case 'bing.be':
				case 'bing.nl':
					
					$crawler = new BingCrawler(
							$q->result->keywords,
							$q->result->search_engine,
							$q->result->domain);
				
					break;
			
			}
			
			$crawler->setInterface($this->currentInterface);
		
			$positionFound = true;
			$i = 0;
			
			$keywordQueueStart->is_parsing = 1;
			$keywordQueueStart->save();
		
			while($positionFound) {
					
				$crawler->parse();
				$positionFound = $crawler->domTreeFromResult();
				
				if(!$crawler->getResult())
				{
					$crawler->setInterface($this->getNextInterface());
					// back on the same page
					$crawler->rewindStart();
				}
									
				//echo " iterator ".$crawler->getStart();
				//echo $crawler->getResult();
		
				sleep(30);
				$i++;
			}
			
			$keywordQueueStart->is_parsing = 0;
			$keywordQueueStart->save(); 
		
			$q->is_parsed = 1;
			$q->position = $crawler->getPosition();
			$q->save();
		
			$q->result->position = $q->position;
			$q->result->is_parsed = 1;
			$q->result->save();
		
		}	
		
	}
	
	function getNextInterface() 
	{
		
		$next = next($this->interfaces);
		
		if($next === false)
		{
			$next = $this->interfaces[0];	
		}
		
		$this->currentInterface = $next;

		return $this->currentInterface;
		
	}
	
	function hasQueue() {
		return KeywordQueue::model()->count() > 0;
	}
	
	function addQueue() {
	
		// how many keywords to put in the keyword queue
		$maxKeywords = 10;
		$count = KeywordQueue::model()->count();
		$countParsed = KeywordQueue::model()->count("is_parsed = :is_parsed", array(':is_parsed' => 1));
			
		if($this->hasQueue()) {
			if($countParsed > 0 &&  $count > 0 && $count == $countParsed) {
				// delete the queue and add new keywords
				KeywordQueue::model()->deleteAll();
	
				$keywordQueueStart = KeywordQueueStart::model()->find();
				$keywordQueueStart->start_at += $maxKeywords;
				$keywordQueueStart->save();
				 
				$criteriaParams = array(
						'order'=> 'id ASC',
						'limit' => $maxKeywords,
						'offset' => $keywordQueueStart->start_at,
						'condition' => 'is_parsed IS NULL'
				);
				 
				$this->addKeywords($criteriaParams);
			}
	
		} else {
	
			$criteriaParams = array(
					'order'=> 'id ASC',
					'limit' => $maxKeywords,
					'offset' => 0,
					'condition' => 'is_parsed IS NULL'
					
			);
	
			$this->addKeywords($criteriaParams);
			
			KeywordQueueStart::model()->deleteAll();
	
			$keywordQueueStart = new KeywordQueueStart();
			$keywordQueueStart->start_at = 0;
			$keywordQueueStart->end_at = $maxKeywords;
			$keywordQueueStart->save();
		}
	
			
	
			
	}
	
	public function addKeywords($criteriaParams) {
	
		$criteria = new CDbCriteria($criteriaParams);
			
		//$keywords = Keyword::model()->findAll($criteria);
		$results = Positions::model()->findAll($criteria);
	 
		if($results) {
			foreach($results as $result) {
					
				$queue = new KeywordQueue();
				$queue->is_parsed  = 0;
				$queue->keyword = $result->keywords->keyword;
				$queue->keyword_id = $result->keywords->id;
				$queue->result_id = $result->id;
				$queue->position = 0;
				$queue->save();
	
			}
		}
	
	}
}