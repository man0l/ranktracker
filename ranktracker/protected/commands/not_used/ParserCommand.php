<?php

class ParserCommand extends CConsoleCommand 
{
	
	private $proxy;
	private $proxyPort;
	private $proxies;
	
	public function run ($args) 
	{
		
	 	$this->getProxies();
	 	$this->getProxies('socks4');
	 	$this->getProxies('socks5');
		
	}
	
	function getProxies($type = 'http') 
	{
	
		
				
		switch($type){
			default:
			case 'http':
				 $result = $this->getHttp();
				 break;
			case 'socks4':
				$result = $this->getSocks4();
				break;
			case 'socks5':
				$result = $this->getSocks5();
				break;			
		}
		
		
		foreach($result->body->data as $proxy) {
			// check for existance
			$check = Proxies::model()->findByAttributes(array('ip_address' => $proxy->Proxy->ip, 'port' => $proxy->Proxy->portNum ));
		
			$pingOk = $this->pingGoogle($proxy->Proxy->ip, $proxy->Proxy->portNum, $proxy->Proxy->protocol);
			
			if(!$check && $pingOk) {
				$prox = new Proxies();
				$prox->proxy = $proxy->Proxy->ip.":".$proxy->Proxy->portNum;
				$prox->ip_address = $proxy->Proxy->ip;
				$prox->port = $proxy->Proxy->portNum;
				$prox->is_valid = 1;
				$prox->created_at = date("Y-m-d H:i:s");
				$prox->is_banned = 0;
				if($proxy->Proxy->protocol == "http")
					$prox->proxy_type = 0;
				else if($proxy->Proxy->protocol == "socks4")
					$prox->proxy_type = 4;
				else if ($proxy->Proxy->protocol == "socks5")
					$prox->proxy_type = 5;
				$prox->valid_retries = 0; 
				
				$prox->save();
			}
		}
		
	}
	
	function getSocks4() {
		
		$url = "http://ninjaproxies.com/proxies/api";
		return Unirest::get($url, 
							   array(),
							   array('key' => '139154846581730981139154846513915484657328766', 
							   		 'protocol' => 'socks4',
							   		 'alive' => 1, 
							   		 'order' => 'random',
									 'limit' => 1000
								));
		
	}
	
	function getSocks5() {
		$url = "http://ninjaproxies.com/proxies/api";
		return Unirest::get($url,
				array(),
				array('key' => '139154846581730981139154846513915484657328766',
						'protocol' => 'socks5',
						'alive' => 1,
						'order' => 'random',
						'limit' => 1000
				));
		
	}
	
	function getHttp() {
		
		$url = "http://ninjaproxies.com/proxies/api";
		return Unirest::get($url,
				array(),
				array('key' => '139154846581730981139154846513915484657328766',
						'protocol' => 'http',
						'alive' => 1,
						'order' => 'random',
						'limit' => 1000
				));
		
	}
	
	function pingGoogle($proxy, $port, $type) {
		
		$working = 0;
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, "http://google.com");
			
		curl_setopt ($ch, CURLOPT_HEADER, 0);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt ($ch, CURLOPT_PROXY, trim ($proxy));
		curl_setopt	($ch, CURLOPT_PROXYPORT, $port);		
		curl_setopt ($ch, CURLOPT_PROXYTYPE, $type);
		
		$result = curl_exec($ch);
		$error = curl_error($ch);
		
		$info = curl_getinfo($ch);
		
		if($info ['http_code'] == 301)
		{
			// it works!
			$working = 1;
		}
		
		return $working;
	}	
	
	
}