<?php

defined('YII_DEBUG') or define('YII_DEBUG',true);

// include Yii bootstrap file
$yii=dirname(__FILE__).'/../yii.php';

// create application instance and run
$config=dirname(__FILE__).'/protected/config/console.php';
require_once($yii);
Yii::createConsoleApplication($config)->run();
